/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50537
Source Host           : localhost:3306
Source Database       : easiadb

Target Server Type    : MYSQL
Target Server Version : 50537
File Encoding         : 65001

Date: 2016-03-15 00:05:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `it_bdatarel`
-- ----------------------------
DROP TABLE IF EXISTS `it_bdatarel`;
CREATE TABLE `it_bdatarel` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `datatkey` varchar(255) NOT NULL DEFAULT '',
  `templid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `key` varchar(255) NOT NULL DEFAULT '',
  `attr` varchar(255) NOT NULL DEFAULT '',
  `default` varchar(250) NOT NULL DEFAULT '',
  `show` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `readonly` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `tab` int(11) NOT NULL DEFAULT '0',
  `sort` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `datatkey` (`datatkey`),
  KEY `templid` (`templid`),
  KEY `key` (`key`)
) ENGINE=MyISAM AUTO_INCREMENT=687 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_bdatarel
-- ----------------------------
INSERT INTO `it_bdatarel` VALUES ('287', 'html', '1', '', 'texthtml', '', '', '1', 'Текст', '0', '0', '1');
INSERT INTO `it_bdatarel` VALUES ('339', 'date', '36', '', 'date', '', '', '1', 'Дата', '0', '0', '1');
INSERT INTO `it_bdatarel` VALUES ('336', 'text', '36', '', 'name', '', '', '1', 'Имя', '0', '0', '2');
INSERT INTO `it_bdatarel` VALUES ('594', 'text', '79', '', 'name', '', '', '1', 'ФИО', '0', '0', '1');
INSERT INTO `it_bdatarel` VALUES ('334', 'html', '35', '', 'text', '', '', '0', 'Текст', '0', '0', '3');
INSERT INTO `it_bdatarel` VALUES ('333', 'text', '35', '', 'name', '', '', '1', 'Заголовок', '0', '0', '2');
INSERT INTO `it_bdatarel` VALUES ('332', 'date', '35', '', 'date', '', '', '1', 'Дата', '0', '0', '1');
INSERT INTO `it_bdatarel` VALUES ('341', 'text', '36', '', 'phone', '', '', '1', 'Телефон', '0', '0', '3');
INSERT INTO `it_bdatarel` VALUES ('350', 'imageload', '35', '', 'photo', '', '', '0', 'Фото', '0', '0', '4');
INSERT INTO `it_bdatarel` VALUES ('425', 'text', '54', '', 'sname', '', '', '1', 'Служебное название', '0', '0', '2');
INSERT INTO `it_bdatarel` VALUES ('424', 'text', '54', '', 'name', '', '', '1', 'Название', '0', '0', '1');
INSERT INTO `it_bdatarel` VALUES ('457', 'text', '59', '', 'name', '', '', '1', 'Название', '0', '0', '1');
INSERT INTO `it_bdatarel` VALUES ('458', 'file', '59', '', 'image', '', '', '0', 'Изображение', '0', '0', '2');
INSERT INTO `it_bdatarel` VALUES ('459', 'text', '59', '', 'href', '', '', '1', 'Cсылка', '0', '0', '3');
INSERT INTO `it_bdatarel` VALUES ('466', 'text', '54', '', 'unit', '', '', '1', 'Единица измерения', '0', '0', '3');
INSERT INTO `it_bdatarel` VALUES ('599', 'text', '79', '', 'login', '', '', '0', 'Логин', '0', '1', '1');
INSERT INTO `it_bdatarel` VALUES ('600', 'text', '79', 'hidden:', 'password', '', '', '0', 'Пароль', '0', '1', '2');
INSERT INTO `it_bdatarel` VALUES ('614', 'text', '79', '', 'email', '', '', '1', 'E-mail', '0', '0', '2');
INSERT INTO `it_bdatarel` VALUES ('616', 'checkbox', '79', '', 'active', '', '0', '1', 'Активирован', '0', '0', '3');
INSERT INTO `it_bdatarel` VALUES ('661', 'text', '102', '', 'name', '', '', '1', 'Название', '0', '0', '1');
INSERT INTO `it_bdatarel` VALUES ('662', 'text', '102', '', 'url', '', '', '1', 'Ссылка', '0', '0', '2');
INSERT INTO `it_bdatarel` VALUES ('664', 'text', '103', '', 'name', '', '', '1', 'Название', '0', '0', '1');
INSERT INTO `it_bdatarel` VALUES ('665', 'text', '103', '', 'format', '', '', '0', 'Формат', '0', '0', '2');
INSERT INTO `it_bdatarel` VALUES ('666', 'file', '103', '', 'file', '', '', '0', 'Файл', '0', '0', '3');
INSERT INTO `it_bdatarel` VALUES ('667', 'text', '104', '', 'name', '', '', '0', 'Название', '0', '0', '1');
INSERT INTO `it_bdatarel` VALUES ('668', 'imageload', '104', 'resize:719*719', 'image', '', '', '0', 'Файл', '0', '0', '2');
INSERT INTO `it_bdatarel` VALUES ('669', 'text', '105', '', 'name', '', '', '1', 'Название', '0', '0', '1');
INSERT INTO `it_bdatarel` VALUES ('670', 'text', '105', '', 'country', '', '', '0', 'Страна', '0', '0', '2');
INSERT INTO `it_bdatarel` VALUES ('671', 'text', '105', '', 'site', '', '', '0', 'Сайт', '0', '0', '3');
INSERT INTO `it_bdatarel` VALUES ('672', 'counter', '105', '', 'text', '', '', '0', 'Описание', '0', '0', '4');
INSERT INTO `it_bdatarel` VALUES ('673', 'imageload', '105', '', 'logo', '', '', '0', 'Логотип', '0', '0', '5');
INSERT INTO `it_bdatarel` VALUES ('674', 'text', '106', '', 'name', '', '', '1', 'Название', '0', '0', '1');
INSERT INTO `it_bdatarel` VALUES ('675', 'html', '106', '', 'text', '', '', '0', 'Описание', '0', '0', '2');
INSERT INTO `it_bdatarel` VALUES ('676', 'mselect', '106', 'b:name:vis:41:filelist', 'files', '', '', '0', 'Документы', '0', '0', '3');
INSERT INTO `it_bdatarel` VALUES ('677', 'imageload', '106', '', 'logo1', '', '', '0', 'Логотип 1', '0', '0', '4');
INSERT INTO `it_bdatarel` VALUES ('678', 'imageload', '106', '', 'logo2', '', '', '0', 'Логотип 2', '0', '0', '5');
INSERT INTO `it_bdatarel` VALUES ('685', 'text', '108', '', 'position', '', '', '1', 'Должность', '0', '0', '2');
INSERT INTO `it_bdatarel` VALUES ('684', 'text', '108', '', 'name', '', '', '1', 'Имя', '0', '0', '1');
INSERT INTO `it_bdatarel` VALUES ('683', 'imageload', '106', '', 'background', '', '', '0', 'Фон', '0', '0', '6');
INSERT INTO `it_bdatarel` VALUES ('686', 'text', '108', '', 'mail', '', '', '1', 'Почта', '0', '0', '3');

-- ----------------------------
-- Table structure for `it_btemplates`
-- ----------------------------
DROP TABLE IF EXISTS `it_btemplates`;
CREATE TABLE `it_btemplates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `canadd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `candel` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `canedit` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `cancopy` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `canmove` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `canhide` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `visible` tinyint(4) NOT NULL DEFAULT '1',
  `seo` tinyint(2) NOT NULL DEFAULT '0',
  `virtual` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `key` (`key`)
) ENGINE=MyISAM AUTO_INCREMENT=109 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_btemplates
-- ----------------------------
INSERT INTO `it_btemplates` VALUES ('1', 'texthtml', 'Текст', '1', '1', '1', '1', '1', '1', '1', '0', '1');
INSERT INTO `it_btemplates` VALUES ('36', 'os', 'Обратный звонок', '0', '1', '1', '0', '0', '1', '1', '0', '1');
INSERT INTO `it_btemplates` VALUES ('35', 'news', 'Новость', '0', '1', '1', '1', '0', '1', '1', '1', '0');
INSERT INTO `it_btemplates` VALUES ('54', 'spec', 'Характеристика', '0', '1', '1', '0', '0', '1', '1', '0', '1');
INSERT INTO `it_btemplates` VALUES ('79', 'user', 'Пользователь', '0', '1', '1', '0', '0', '0', '1', '0', '1');
INSERT INTO `it_btemplates` VALUES ('102', 'mainmenu', 'Главное меню', '0', '0', '1', '0', '0', '0', '1', '0', '1');
INSERT INTO `it_btemplates` VALUES ('103', 'filelist', 'Файлы', '0', '0', '1', '0', '0', '0', '1', '0', '1');
INSERT INTO `it_btemplates` VALUES ('104', 'imagelist', 'Картинки', '0', '0', '1', '0', '0', '0', '1', '0', '1');
INSERT INTO `it_btemplates` VALUES ('105', 'partnerslist', 'Наши партнеры', '0', '0', '1', '0', '0', '0', '1', '0', '1');
INSERT INTO `it_btemplates` VALUES ('106', 'projectlist', 'Наши проекты', '0', '0', '1', '0', '0', '1', '1', '1', '0');
INSERT INTO `it_btemplates` VALUES ('108', 'contactlist', 'Контакты', '1', '1', '1', '1', '1', '1', '1', '0', '1');

-- ----------------------------
-- Table structure for `it_b_contactlist`
-- ----------------------------
DROP TABLE IF EXISTS `it_b_contactlist`;
CREATE TABLE `it_b_contactlist` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `blockparent` int(12) DEFAULT NULL,
  `sort` int(12) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `mail` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_b_contactlist
-- ----------------------------
INSERT INTO `it_b_contactlist` VALUES ('1', '47', null, '1', '1', '2016-03-14 23:11:18', 'Тимур Шонематов', 'Генеральный директор', 'info@vostok13.ru');
INSERT INTO `it_b_contactlist` VALUES ('2', '47', null, '2', '1', '2016-03-14 23:11:51', 'Алексей Плавинский', 'Заместитель директора', 'info@vostok13.ru');
INSERT INTO `it_b_contactlist` VALUES ('3', '47', null, '3', '1', '2016-03-14 23:12:16', 'Елена Ефремова', 'Руководитель отдела маркетинга', 'info@vostok13.ru');

-- ----------------------------
-- Table structure for `it_b_filelist`
-- ----------------------------
DROP TABLE IF EXISTS `it_b_filelist`;
CREATE TABLE `it_b_filelist` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `blockparent` int(12) DEFAULT NULL,
  `sort` int(12) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(255) DEFAULT NULL,
  `format` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_b_filelist
-- ----------------------------
INSERT INTO `it_b_filelist` VALUES ('1', '39', null, '1', '1', '2016-03-14 16:17:36', 'Скачать документ (Rus)', 'pdf', '50520971457950656.pdf');
INSERT INTO `it_b_filelist` VALUES ('2', '41', null, '2', '1', '2016-03-14 19:06:55', 'ТЗ', 'pdf', '101690741457960815.pdf');
INSERT INTO `it_b_filelist` VALUES ('3', '41', null, '3', '1', '2016-03-14 19:07:21', 'Договор', 'pdf', '116915901457960841.pdf');

-- ----------------------------
-- Table structure for `it_b_imagelist`
-- ----------------------------
DROP TABLE IF EXISTS `it_b_imagelist`;
CREATE TABLE `it_b_imagelist` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `blockparent` int(12) DEFAULT NULL,
  `sort` int(12) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(255) DEFAULT NULL,
  `image` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_b_imagelist
-- ----------------------------
INSERT INTO `it_b_imagelist` VALUES ('1', '39', null, '1', '1', '2016-03-14 16:28:06', 'Наш Завод', ';27707101457951265.jpg;');
INSERT INTO `it_b_imagelist` VALUES ('2', '39', null, '2', '1', '2016-03-14 16:28:30', 'Наш Штаб', ';57449231457951298.jpg;');
INSERT INTO `it_b_imagelist` VALUES ('3', '39', null, '3', '1', '2016-03-14 16:28:54', 'Наш писарь', ';106458611457951317.jpg;');
INSERT INTO `it_b_imagelist` VALUES ('4', '39', null, '4', '1', '2016-03-14 16:29:15', 'Наш автопарк', ';11725031457951341.jpg;');

-- ----------------------------
-- Table structure for `it_b_mainmenu`
-- ----------------------------
DROP TABLE IF EXISTS `it_b_mainmenu`;
CREATE TABLE `it_b_mainmenu` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `blockparent` int(12) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `sort` int(12) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_b_mainmenu
-- ----------------------------
INSERT INTO `it_b_mainmenu` VALUES ('1', '1', null, '1', '2016-03-14 15:10:21', 'О компании', '/about/', '2');
INSERT INTO `it_b_mainmenu` VALUES ('2', '1', null, '1', '2016-03-14 15:10:21', 'Партнеры', '/partners/', '3');
INSERT INTO `it_b_mainmenu` VALUES ('3', '1', null, '1', '2016-03-14 15:10:21', 'Проекты', '/projects/', '4');
INSERT INTO `it_b_mainmenu` VALUES ('4', '1', null, '1', '2016-03-14 15:10:21', 'Новости', '/news/', '5');
INSERT INTO `it_b_mainmenu` VALUES ('5', '1', null, '1', '2016-03-14 15:10:21', 'Контакты', '/contacts/', '7');

-- ----------------------------
-- Table structure for `it_b_news`
-- ----------------------------
DROP TABLE IF EXISTS `it_b_news`;
CREATE TABLE `it_b_news` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `blockparent` int(12) DEFAULT NULL,
  `sort` int(12) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `text` text,
  `utitle` varchar(255) DEFAULT NULL,
  `udescription` text,
  `ukeywords` text,
  `uurl` varchar(255) DEFAULT NULL,
  `photo` text,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_b_news
-- ----------------------------
INSERT INTO `it_b_news` VALUES ('1', '46', null, '1', '1', '2016-03-14', 'ИРАН И РОССИЯ РАСШИРЯЮТ СОТРУДНИЧЕСТВО В ОБЛАСТИ ТРАНСПОРТА', '<p><strong>Иран и Россия договорились расширять двустороннее сотрудничества в области морского, наземного и воздушного транспорта, и пути расширения этого сотрудничества стали темой заседания Ирано-Российской рабочей группы по транспорту в Москве.</strong></p>\r\n<p>По информации посольства Ирана в Москве, члены иранской делегации во главе с заместителем министра дорог и городского строительства по вопросам планирования и экономики Амира Амини на заседании рабочей группы обсудили с российской стороной проблемы электрификации железных дорог, обменялись мнениями по поводу проблем, с которыми сталкиваются иранские и российские морские суда, и обсудили вопрос углубления Волжского канала. Другими темами, по которым в ходе переговоров между двумя сторонами достигнуты договоренности, были проведение ирано-российских консультаций по вопросам транспорта, в частности организация регулярного автобусного пассажирского сообщения между двумя странами, активная эксплуатация транспортного коридора &laquo;Север &ndash; Юг&raquo;, создание совместной рабочей группы по вопросам досмотра морских судов в портах друг друга и расширение сотрудничества в области воздушного транспорта.</p>\r\n<p>Кроме того, в ходе визита иранской делегации во главе с Амиром Амини состоялась двусторонняя встреча на уровне заместителей министров транспорта двух стран, а по результатом 7-го заседания рабочей группы по транспорту был подписан совместный меморандум о взаимопонимании.таном и другими, и приветствует дальнейшее укрепление связей с Российской Федерацией.</p>', 'Новость 1', 'Новость 1', 'Новость 1', 'news/Новость 1/', ';26630751457971353.jpg;', '2016-03-14 22:02:58');
INSERT INTO `it_b_news` VALUES ('2', '46', null, '2', '1', '2016-03-14', 'ВЕРТОЛЕТЫ', '<p><strong>Вертолет и Россия договорились расширять двустороннее сотрудничества в области морского, наземного и воздушного транспорта, и пути расширения этого сотрудничества стали темой заседания Ирано-Российской рабочей группы по транспорту в Москве.</strong></p>\r\n<p>По информации посольства Ирана в Москве, члены иранской делегации во главе с заместителем министра дорог и городского строительства по вопросам планирования и экономики Амира Амини на заседании рабочей группы обсудили с российской стороной проблемы электрификации железных дорог, обменялись мнениями по поводу проблем, с которыми сталкиваются иранские и российские морские суда, и обсудили вопрос углубления Волжского канала. Другими темами, по которым в ходе переговоров между двумя сторонами достигнуты договоренности, были проведение ирано-российских консультаций по вопросам транспорта, в частности организация регулярного автобусного пассажирского сообщения между двумя странами, активная эксплуатация транспортного коридора &laquo;Север &ndash; Юг&raquo;, создание совместной рабочей группы по вопросам досмотра морских судов в портах друг друга и расширение сотрудничества в области воздушного транспорта.</p>\r\n<p>Кроме того, в ходе визита иранской делегации во главе с Амиром Амини состоялась двусторонняя встреча на уровне заместителей министров транспорта двух стран, а по результатом 7-го заседания рабочей группы по транспорту был подписан совместный меморандум о взаимопонимании.таном и другими, и приветствует дальнейшее укрепление связей с Российской Федерацией.</p>', 'Вертолет', 'Вертолет', 'Вертолет', 'news/Вертолет/', ';112214781457969330.jpg;', '2016-03-14 21:29:05');
INSERT INTO `it_b_news` VALUES ('3', '46', null, '3', '1', '2016-03-14', 'kjhkjtgy', '<p>gkjgdsfsdfsdfsd sdf sdf sd fs</p>', 'kshdgkhg', 'kjhgkjsdhgfkjhg', 'kjhg', 'news/skajdh/', ';108694181457971259.jpg;', '2016-03-14 22:01:11');
INSERT INTO `it_b_news` VALUES ('4', '46', null, '4', '1', '2016-03-14', 'jhgkjhgk', '<p>jhgsdgfsudgfsfd</p>', 'ysdgjhbvmkfg', 'ifgkugfk', 'gfku', 'news/uyuysdt/', ';58884251457971291.jpg;', '2016-03-14 22:01:42');
INSERT INTO `it_b_news` VALUES ('5', '46', null, '5', '1', '2016-03-14', 'выаываыва', 'ываываа', 'рпорпорп', 'орпорп', 'орпорп', 'news/u33ysdt/', ';58884251457971291.jpg;', '2016-03-14 22:07:15');
INSERT INTO `it_b_news` VALUES ('6', '46', null, '6', '1', '2016-03-14', 'kdhfgjhdfgkjh', '<p>gkjhgskdjfgskjdfgsdf</p>', 'jkhgdskjhg', 'kjhgkjdhgsf', 'jhgsdf', 'news/sdkfhjg/', ';39321061457971661.jpg;', '2016-03-14 22:07:53');
INSERT INTO `it_b_news` VALUES ('7', '46', null, '7', '1', '2016-03-14', 'kjdsfhksjdhfgkjhg', '<p>jhsdgiufytsdiuyft</p>', 'ytdsiufy', 'yutiuyti', 'uytouyt', 'news/lkyhj/', ';147211791457971685.jpg;', '2016-03-14 22:08:15');
INSERT INTO `it_b_news` VALUES ('8', '46', null, '8', '1', '2016-03-14', 'kjdshfkjk', '<p>jhgkjdhgsfsdf</p>', 'hgkjhgkjh', 'gkjhgkjhg', 'kjhgkj', 'news/hjgjhvk/', ';62677701457973613.jpg;', '2016-03-14 22:40:17');
INSERT INTO `it_b_news` VALUES ('9', '46', null, '9', '1', '2016-03-14', 'kjhklhiytiuytiyri7r578', '<p>gsdkjfsdiuyfgs</p>', 'tiuy59876587', '6764865ityfdu6', 'ftfikf', 'news/kjhgfgiughjyu/', ';111634981457973627.jpg;', '2016-03-14 22:40:36');
INSERT INTO `it_b_news` VALUES ('10', '46', null, '10', '1', '2016-03-14', 'kjgkgiuytiuytiut', '<p>iuytiuytiuytiuy</p>', 'ytruygf', 'ygf', 'khgfkfgkjhg', 'news/mvbmnvjhfuytru/', ';104863691457973650.jpg;', '2016-03-14 22:40:59');

-- ----------------------------
-- Table structure for `it_b_os`
-- ----------------------------
DROP TABLE IF EXISTS `it_b_os`;
CREATE TABLE `it_b_os` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `blockparent` int(12) DEFAULT NULL,
  `sort` int(12) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_b_os
-- ----------------------------
INSERT INTO `it_b_os` VALUES ('1', '14', null, '1', '1', 'fsa232', '2015-02-20', 'fsa', '2015-02-20 11:03:35');

-- ----------------------------
-- Table structure for `it_b_partnerslist`
-- ----------------------------
DROP TABLE IF EXISTS `it_b_partnerslist`;
CREATE TABLE `it_b_partnerslist` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `blockparent` int(12) DEFAULT NULL,
  `sort` int(12) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `site` varchar(255) DEFAULT NULL,
  `text` text,
  `logo` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_b_partnerslist
-- ----------------------------
INSERT INTO `it_b_partnerslist` VALUES ('1', '40', null, '1', '1', '2016-03-14 16:48:20', 'Группа компаний \"роспроминжиниринг\"', 'Россия', 'http://www.qk-rpi.ru', 'ГК «РосПромИнжиниринг» является лидером некоммерческого партнерства «Инжиниринговый Кластер Инфраструктурных проектов» и Кластера Импортозамещения для нужд ТЭК РФ. По данным объединениям совместно с нашими партнерами мы участвуем в модернизации и строител', ';151135421457952486.png;');
INSERT INTO `it_b_partnerslist` VALUES ('2', '40', null, '2', '1', '2016-03-14 16:58:21', 'Банк', 'США', 'http://chase.com', 'Вылоар ылвоарывла иылв рпывар плывоа ыво апывло ичлсомп шчсгнме шчгсм рлчрпсм 8ешчщм рчом рчдсломрл пчсм рчлсомр пчлсмо ьчсом рчдсолмр пчлсмгр.', ';25728951457953093.png;');

-- ----------------------------
-- Table structure for `it_b_projectlist`
-- ----------------------------
DROP TABLE IF EXISTS `it_b_projectlist`;
CREATE TABLE `it_b_projectlist` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `blockparent` int(12) DEFAULT NULL,
  `sort` int(12) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(255) DEFAULT NULL,
  `text` text,
  `files` text,
  `logo1` text,
  `logo2` text,
  `utitle` varchar(255) DEFAULT NULL,
  `udescription` text,
  `ukeywords` text,
  `uurl` varchar(255) DEFAULT NULL,
  `background` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_b_projectlist
-- ----------------------------
INSERT INTO `it_b_projectlist` VALUES ('1', '41', null, '1', '1', '2016-03-14 19:35:29', 'Роснефть', '<p>kshdgfk jsdhgf jsdgf sjd fgksdf gjsdhf gsjdf glksd hflkdj hflskdjfh lk&nbsp;kshdgfk jsdhgf jsdgf sjd fgksdf gjsdhf gsjdf glksd hflkdj hflskdjfh lk&nbsp;kshdgfk jsdhgf jsdgf sjd fgksdf gjsdhf gsjdf glksd hflkdj hflskdjfh lk&nbsp;kshdgfk jsdhgf jsdgf sjd fgksdf gjsdhf gsjdf glksd hflkdj hflskdjfh lk&nbsp;kshdgfk jsdhgf jsdgf sjd fgksdf gjsdhf gsjdf glksd hflkdj hflskdjfh lk</p>', '2', '126085601457955358.jpg', '86709471457955370.jpg', 'Роснефть', 'Роснефть', 'Роснефть', 'projects/rosneft/', '');
INSERT INTO `it_b_projectlist` VALUES ('3', '42', null, '2', '1', '2016-03-14 19:08:00', 'Роснефть', '<p>лвыораыдлв ораыдлво арыл орчдшм рсшм пчсдло мпищрс мпчщш гмпчщш рпывщл ривщл ипвыщш пгпвыщш гывлд мочсдлм рпщшсг мпчщшсгм зпяч</p>', '2;3', '160981021457956298.png', '80660271457956306.png', 'Роснефть', 'Роснефть', 'Роснефть', 'projects/neftyanaya-sfera/rosneft.html/', '8358081457957641.jpg');
INSERT INTO `it_b_projectlist` VALUES ('4', '43', null, '3', '1', '2016-03-14 19:48:21', 'Газпром', '<p>лоы апывлоап лсм чгншмн чсопч шдчом очшсм ьрщочянспм щшзгнс8 е9чсн м8ч67ея нашрпыа шщч рилоыгп рмжылврм жчл чясоырваыджлвои лшдщр&nbsp;</p>', '2', '82093081457956456.png', '127117941457956466.png', 'Газпром', 'Газпром', 'Газпром', 'projects/gazovaya-sfera/gazprom.html/', '92468951457957681.jpg');
INSERT INTO `it_b_projectlist` VALUES ('5', '42', null, '4', '1', '2016-03-14 20:16:01', 'BP', '<p>sdfhlskdjf gsjdk fgkjxv gxljcv glcj vgxclkj vgxclkjv gxlkv gdl kdgfo iugsdv jbcxkvgxochlvk/ bzjkgl bkvzuys</p>', '2;3', '163738821457964871.png', '163477711457964878.png', 'bp', 'bp', 'bp', 'projects/neftyanaya-sfera/bp.html/', '89119641457964898.jpg');
INSERT INTO `it_b_projectlist` VALUES ('6', '42', null, '5', '1', '2016-03-14 20:21:23', 'sdkfhh/', '<p>oisudyfsiudf</p>', '', '48198111457965184.png', '154145911457965191.png', 'sdkjfgkhsdg', 'kjgksdjgk', 'jhgkjhg', 'projects/neftyanaya-sfera/sdfkhj/', '139982071457965199.jpg');

-- ----------------------------
-- Table structure for `it_b_spec`
-- ----------------------------
DROP TABLE IF EXISTS `it_b_spec`;
CREATE TABLE `it_b_spec` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `blockparent` int(12) DEFAULT NULL,
  `sort` int(12) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sname` varchar(255) DEFAULT NULL,
  `unit` varchar(255) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=75 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_b_spec
-- ----------------------------
INSERT INTO `it_b_spec` VALUES ('1', null, '48', '1', '1', 'Артикул', '1455-54', null, '0000-00-00 00:00:00');
INSERT INTO `it_b_spec` VALUES ('2', null, '48', '2', '1', 'Цвет', 'зеленый', null, '0000-00-00 00:00:00');
INSERT INTO `it_b_spec` VALUES ('3', null, '48', '3', '1', 'Материал', 'Лесоматериал', null, '0000-00-00 00:00:00');
INSERT INTO `it_b_spec` VALUES ('4', null, '49', '4', '1', 'Артикул', '444-554', null, '0000-00-00 00:00:00');
INSERT INTO `it_b_spec` VALUES ('5', null, '49', '5', '1', 'Цвет', 'желтый', null, '0000-00-00 00:00:00');
INSERT INTO `it_b_spec` VALUES ('6', null, '49', '6', '1', 'Материалы', 'кожа/дуб/подсолнечник', null, '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `it_b_texthtml`
-- ----------------------------
DROP TABLE IF EXISTS `it_b_texthtml`;
CREATE TABLE `it_b_texthtml` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `blockparent` int(12) DEFAULT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(12) DEFAULT NULL,
  `texthtml` text,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_b_texthtml
-- ----------------------------
INSERT INTO `it_b_texthtml` VALUES ('77', null, '26', '1', '1', '<p>Привет</p>', '0000-00-00 00:00:00');
INSERT INTO `it_b_texthtml` VALUES ('78', '39', null, '1', '2', '<p>Подбор промышленных технологий, сервисных услуг, производителей продуктов питания, товаров народного потребления для стран: Россия, Иран, стран Ближнего Востока, Казахстана, Таджикистана, Узбекистана по Вашему запросу;</p>', '2016-03-14 16:06:48');
INSERT INTO `it_b_texthtml` VALUES ('79', '39', null, '1', '3', '<p>Содействие в заключении контрактов на поставку товаров, технологий и услуг, контроль своевременного осуществления платежей между контрагентами;</p>', '2016-03-14 16:07:37');
INSERT INTO `it_b_texthtml` VALUES ('80', '39', null, '1', '4', '<p>Организация совместных туров, строительство отелей, сооружений и различных объектов для туризма и торговли с привлечением инвестиций, развитие партнерства между частными экономическими секторами Ирана, России и СНГ. <br /> <br />!!!ЭТИ БЛОКИ создавать в режиме исходного кода!!</p>', '2016-03-14 16:09:04');

-- ----------------------------
-- Table structure for `it_b_user`
-- ----------------------------
DROP TABLE IF EXISTS `it_b_user`;
CREATE TABLE `it_b_user` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `blockparent` int(12) DEFAULT NULL,
  `sort` int(12) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `login` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_b_user
-- ----------------------------

-- ----------------------------
-- Table structure for `it_categories`
-- ----------------------------
DROP TABLE IF EXISTS `it_categories`;
CREATE TABLE `it_categories` (
  `name` varchar(255) NOT NULL DEFAULT '',
  `template` varchar(255) NOT NULL DEFAULT '0',
  `templateinc` text NOT NULL,
  `sort` bigint(20) unsigned NOT NULL DEFAULT '0',
  `visible` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL DEFAULT '',
  `insertblocks` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `template` (`template`),
  KEY `parent` (`parent`),
  KEY `key` (`key`),
  KEY `visible` (`visible`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_categories
-- ----------------------------
INSERT INTO `it_categories` VALUES ('О компании', 'first', '', '1', '1', '0', '1', 'o-kompanii', '0   ');
INSERT INTO `it_categories` VALUES ('Блок', 'adminblockedit', '', '3', '0', '2', '3', 'blockedit', '0');
INSERT INTO `it_categories` VALUES ('Шаблоны блоков', 'adminblocktemplate', '', '5', '0', '2', '4', 'blocktemplate', '0');
INSERT INTO `it_categories` VALUES ('Шаблоны папок', 'admincattemplate', '', '4', '0', '2', '5', 'cattemplate', '0');
INSERT INTO `it_categories` VALUES ('Папка', 'admincatedit', '', '2', '0', '2', '6', 'catedit', '0');
INSERT INTO `it_categories` VALUES ('Админка', 'manage', '', '22', '0', '1', '2', 'manage', '0');
INSERT INTO `it_categories` VALUES ('Настройки сайта', 'settings', '', '20', '0', '1', '12', '', '0');
INSERT INTO `it_categories` VALUES ('Типы данных', 'admindatatypes', '', '7', '0', '2', '8', 'admindatatypes', '0');
INSERT INTO `it_categories` VALUES ('Пользователи', 'adminusers', '', '8', '0', '2', '9', 'users', '0');
INSERT INTO `it_categories` VALUES ('Модуль Заказы', 'adminmoduleorders', '', '9', '0', '2', '10', 'moduleorders', '0');
INSERT INTO `it_categories` VALUES ('Ошибка 404', 'e404', '', '21', '0', '1', '13', 'error404', '0');
INSERT INTO `it_categories` VALUES ('Заказ звонока', 'os', '', '19', '0', '1', '14', 'os', '0');
INSERT INTO `it_categories` VALUES ('Пользователи', 'users', '', '15', '0', '1', '29', 'users', '0');
INSERT INTO `it_categories` VALUES ('О компании', 'about', '', '23', '0', '1', '39', 'about', '0');
INSERT INTO `it_categories` VALUES ('Партнеры', 'partners', '', '24', '0', '1', '40', 'partners', '0');
INSERT INTO `it_categories` VALUES ('Проекты', 'projects', '', '25', '0', '1', '41', 'projects', '0');
INSERT INTO `it_categories` VALUES ('Нефтяная сфера', 'projects', '', '1', '0', '41', '42', 'neftyanaya-sfera', '0');
INSERT INTO `it_categories` VALUES ('Газовая сфера', 'projects', '', '2', '0', '41', '43', 'gazovaya-sfera', '0');
INSERT INTO `it_categories` VALUES ('Остальные', 'projects', '', '3', '0', '41', '45', 'ostalnye', '0');
INSERT INTO `it_categories` VALUES ('Новости', 'news', '', '26', '0', '1', '46', 'news', '0');
INSERT INTO `it_categories` VALUES ('Контакты', 'contacts', '', '27', '0', '1', '47', 'contacts', '0');

-- ----------------------------
-- Table structure for `it_cdatarel`
-- ----------------------------
DROP TABLE IF EXISTS `it_cdatarel`;
CREATE TABLE `it_cdatarel` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `templid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `datatkey` varchar(255) NOT NULL DEFAULT '',
  `comment` text NOT NULL,
  `key` varchar(255) NOT NULL DEFAULT '',
  `attr` varchar(255) NOT NULL DEFAULT '',
  `default` longblob NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `readonly` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sort` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_cdatarel
-- ----------------------------
INSERT INTO `it_cdatarel` VALUES ('61', '51', 'text', '', 'address', '', '', 'Адрес', '0', '1');
INSERT INTO `it_cdatarel` VALUES ('60', '51', 'text', '', 'code', '', '', 'Код города', '0', '2');
INSERT INTO `it_cdatarel` VALUES ('57', '51', 'text', '', 'phone', '', '', 'Телефон', '0', '3');
INSERT INTO `it_cdatarel` VALUES ('67', '41', 'html', '', 'text', '', '', 'Текст', '0', '1');
INSERT INTO `it_cdatarel` VALUES ('65', '88', 'text', '', 'sitename', '', '', 'Название сайта', '0', '1');
INSERT INTO `it_cdatarel` VALUES ('63', '84', 'text', '', 'block', '', '', 'Блок заказа', '0', '1');
INSERT INTO `it_cdatarel` VALUES ('66', '88', 'text', '', 'email', '', '', 'E-mail для обратной связи', '0', '2');
INSERT INTO `it_cdatarel` VALUES ('74', '88', 'text', '', 'code', '', '', 'Код города', '0', '3');
INSERT INTO `it_cdatarel` VALUES ('75', '88', 'text', '', 'phone', '', '', 'Телефон', '0', '4');
INSERT INTO `it_cdatarel` VALUES ('76', '1', 'text', '', 'seoname', '', '', 'Заголовок SEO', '0', '1');
INSERT INTO `it_cdatarel` VALUES ('77', '1', 'html', '', 'seotext', '', '', 'Текст SEO', '0', '2');
INSERT INTO `it_cdatarel` VALUES ('78', '88', 'text', '', 'copy', '', '', 'Копирайт', '0', '5');
INSERT INTO `it_cdatarel` VALUES ('83', '111', 'text', '', 'page_title', '', '', 'Заголовок на странице', '0', '1');
INSERT INTO `it_cdatarel` VALUES ('84', '111', 'counter', '', 'page_text', '', '', 'Текст на странице', '0', '2');
INSERT INTO `it_cdatarel` VALUES ('85', '111', 'counter', '', 'page_doctext', '', '', 'Текст возле документов', '0', '3');
INSERT INTO `it_cdatarel` VALUES ('86', '112', 'text', '', 'page_title', '', '', 'Заголовок на странице', '0', '1');
INSERT INTO `it_cdatarel` VALUES ('87', '112', 'counter', '', 'page_text', '', '', 'Текст на странице', '0', '2');
INSERT INTO `it_cdatarel` VALUES ('88', '113', 'text', '', 'title', '', '', 'Заголовок', '0', '1');
INSERT INTO `it_cdatarel` VALUES ('89', '113', 'counter', '', 'text', '', '', 'Описание', '0', '2');
INSERT INTO `it_cdatarel` VALUES ('90', '114', 'text', '', 'address', '', '', 'Адрес', '0', '1');
INSERT INTO `it_cdatarel` VALUES ('91', '114', 'text', '', 'phone', '', '', 'Телефон', '0', '2');
INSERT INTO `it_cdatarel` VALUES ('92', '114', 'counter', '', 'map', '', '', 'Карта', '0', '3');
INSERT INTO `it_cdatarel` VALUES ('93', '114', 'text', '', 'admin_mail', '', '', 'Почта Админа', '0', '4');

-- ----------------------------
-- Table structure for `it_ctemplates`
-- ----------------------------
DROP TABLE IF EXISTS `it_ctemplates`;
CREATE TABLE `it_ctemplates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `candel` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `canedit` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `canaddcat` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `canaddbl` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `canmoveto` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `cancopyto` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `canmovefrom` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `cancopyfrom` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `blocktypes` text NOT NULL,
  `key` varchar(255) NOT NULL DEFAULT '',
  `canhide` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `hidestructure` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `cattypes` text NOT NULL,
  `visible` tinyint(4) NOT NULL DEFAULT '1',
  `virtual` tinyint(1) NOT NULL DEFAULT '0',
  `cache` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_ctemplates
-- ----------------------------
INSERT INTO `it_ctemplates` VALUES ('1', 'Главная', '0', '1', '0', '0', '0', '0', '0', '0', 'mainmenu ', 'first', '0', '|h1.html|', '0', '', '1', '0', '0');
INSERT INTO `it_ctemplates` VALUES ('2', 'Типовая внутренняя', '0', '1', '1', '1', '1', '0', '0', '0', 'texthtml ', 'typo', '1', '|h2.html|texthtml', '0', 'typo ', '1', '0', '0');
INSERT INTO `it_ctemplates` VALUES ('35', 'АдминШаблонБлок', '0', '0', '0', '0', '0', '0', '0', '0', '', 'adminblocktemplate', '0', '|manage.html|', '0', '', '1', '1', '0');
INSERT INTO `it_ctemplates` VALUES ('51', 'Номер телефона', '0', '1', '0', '0', '0', '0', '0', '0', '', 'phoneblock', '0', '', '0', '', '1', '1', '0');
INSERT INTO `it_ctemplates` VALUES ('52', 'Карта сайта', '0', '0', '0', '0', '0', '0', '0', '0', '', 'map', '0', '|map.html|', '0', '', '1', '0', '0');
INSERT INTO `it_ctemplates` VALUES ('58', 'Обратная связь', '0', '0', '0', '1', '0', '0', '0', '0', 'os ', 'os', '0', '', '0', '', '1', '1', '0');
INSERT INTO `it_ctemplates` VALUES ('17', 'Админка', '0', '0', '0', '0', '0', '0', '0', '0', '', 'manage', '0', '|manage.html|', '1', '', '1', '1', '0');
INSERT INTO `it_ctemplates` VALUES ('18', 'АдминПапка', '0', '0', '0', '0', '0', '0', '0', '0', '', 'admincatedit', '0', '|manage.html|', '0', '', '1', '1', '0');
INSERT INTO `it_ctemplates` VALUES ('19', 'АдминБлок', '0', '0', '0', '0', '0', '0', '0', '0', '', 'adminblockedit', '0', '|manage.html|', '0', '', '1', '1', '0');
INSERT INTO `it_ctemplates` VALUES ('20', 'АдминШаблонПапка', '0', '0', '0', '0', '0', '0', '0', '0', '', 'admincattemplate', '0', '|manage.html|', '0', '', '1', '1', '0');
INSERT INTO `it_ctemplates` VALUES ('36', 'Админ Файловый менеджер', '0', '0', '0', '0', '0', '0', '0', '0', '', 'adminfm', '0', '|manage.html|', '0', '', '1', '1', '0');
INSERT INTO `it_ctemplates` VALUES ('37', 'АдминТипыДанных', '0', '0', '0', '0', '0', '0', '0', '0', '', 'admindatatypes', '0', '|manage.html|', '0', '', '1', '1', '0');
INSERT INTO `it_ctemplates` VALUES ('40', 'АдминПользователи', '0', '0', '0', '0', '0', '0', '0', '0', '', 'adminusers', '0', '|manage.html|', '0', '', '1', '1', '0');
INSERT INTO `it_ctemplates` VALUES ('41', '404', '0', '1', '0', '0', '0', '0', '0', '0', '', 'e404', '0', '|h2.html|', '0', '', '1', '1', '0');
INSERT INTO `it_ctemplates` VALUES ('89', 'Новости', '0', '1', '0', '1', '1', '0', '0', '0', 'news ', 'news', '0', '|h3.html|', '0', '', '1', '0', '0');
INSERT INTO `it_ctemplates` VALUES ('88', 'Настройки сайта', '0', '1', '0', '0', '0', '0', '0', '0', '', 'settings', '0', '', '0', '', '1', '1', '0');
INSERT INTO `it_ctemplates` VALUES ('82', 'Таблицы характеристик', '0', '0', '1', '0', '0', '0', '0', '0', '', 'tablech', '0', '', '0', 'tablechitem ', '1', '1', '0');
INSERT INTO `it_ctemplates` VALUES ('83', 'Таблица характеристик', '0', '1', '0', '1', '0', '0', '0', '0', 'spec ', 'tablechitem', '1', '', '0', '', '1', '1', '0');
INSERT INTO `it_ctemplates` VALUES ('84', 'АдминМодульЗаказы', '0', '0', '0', '0', '0', '0', '0', '0', '', 'adminmoduleorders', '0', '|manage.html|', '0', '', '1', '1', '0');
INSERT INTO `it_ctemplates` VALUES ('102', 'Пользовататели', '0', '0', '0', '1', '0', '0', '0', '0', 'user ', 'users', '0', '', '0', '', '1', '1', '0');
INSERT INTO `it_ctemplates` VALUES ('111', 'О компании', '0', '0', '0', '1', '0', '0', '0', '0', 'texthtml filelist imagelist ', 'about', '0', '|h2.html|', '0', '', '1', '0', '0');
INSERT INTO `it_ctemplates` VALUES ('112', 'Партнеры', '0', '1', '0', '0', '0', '0', '0', '0', 'partnerslist ', 'partners', '0', '|h2.html|', '0', '', '1', '0', '0');
INSERT INTO `it_ctemplates` VALUES ('113', 'Проекты', '0', '1', '0', '0', '0', '0', '0', '0', 'projectlist ', 'projects', '0', '|h2.html|', '0', '', '1', '0', '0');
INSERT INTO `it_ctemplates` VALUES ('114', 'Контакты', '0', '1', '0', '0', '0', '0', '0', '0', 'contactlist ', 'contacts', '0', '|h2.html|', '0', '', '1', '0', '0');

-- ----------------------------
-- Table structure for `it_c_about`
-- ----------------------------
DROP TABLE IF EXISTS `it_c_about`;
CREATE TABLE `it_c_about` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `utitle` varchar(255) DEFAULT NULL,
  `udescription` text,
  `ukeywords` text,
  `page_title` varchar(255) DEFAULT NULL,
  `page_text` text,
  `page_doctext` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_c_about
-- ----------------------------
INSERT INTO `it_c_about` VALUES ('1', '39', 'About us', 'About us', 'About us', 'О компании East-Asia Consulting', 'Перечень услуг, которые оказывает консалтинговая компания \"East-Asia Consulting\" в рамках сотрудничества стран Россия-Иран (Ближний Восток)', '\"Меморандум о сотрудничестве компании \"East-Asia Consulting\" и холдинга \"Aria Bartar Gharn Paydar\" между странами Россия и Иран в области экономики, промышленности, науки, туризма в частном секторе между двумя странами, передача знаний об управлении для п');

-- ----------------------------
-- Table structure for `it_c_adminblockedit`
-- ----------------------------
DROP TABLE IF EXISTS `it_c_adminblockedit`;
CREATE TABLE `it_c_adminblockedit` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `visible` int(1) DEFAULT NULL,
  `utitle` varchar(255) DEFAULT NULL,
  `udescription` text,
  `ukeywords` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_c_adminblockedit
-- ----------------------------
INSERT INTO `it_c_adminblockedit` VALUES ('1', '218', null, '', '', '');

-- ----------------------------
-- Table structure for `it_c_adminblocktemplate`
-- ----------------------------
DROP TABLE IF EXISTS `it_c_adminblocktemplate`;
CREATE TABLE `it_c_adminblocktemplate` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `visible` int(1) DEFAULT NULL,
  `utitle` varchar(255) DEFAULT NULL,
  `udescription` text,
  `ukeywords` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_c_adminblocktemplate
-- ----------------------------
INSERT INTO `it_c_adminblocktemplate` VALUES ('1', '229', null, '', '', '');

-- ----------------------------
-- Table structure for `it_c_admincatedit`
-- ----------------------------
DROP TABLE IF EXISTS `it_c_admincatedit`;
CREATE TABLE `it_c_admincatedit` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `visible` int(1) DEFAULT NULL,
  `utitle` varchar(255) DEFAULT NULL,
  `udescription` text,
  `ukeywords` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_c_admincatedit
-- ----------------------------
INSERT INTO `it_c_admincatedit` VALUES ('1', '208', null, '', '', '');

-- ----------------------------
-- Table structure for `it_c_admincattemplate`
-- ----------------------------
DROP TABLE IF EXISTS `it_c_admincattemplate`;
CREATE TABLE `it_c_admincattemplate` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `visible` int(1) DEFAULT NULL,
  `utitle` varchar(255) DEFAULT NULL,
  `udescription` text,
  `ukeywords` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_c_admincattemplate
-- ----------------------------
INSERT INTO `it_c_admincattemplate` VALUES ('1', '227', null, '', '', '');

-- ----------------------------
-- Table structure for `it_c_admindatatypes`
-- ----------------------------
DROP TABLE IF EXISTS `it_c_admindatatypes`;
CREATE TABLE `it_c_admindatatypes` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `visible` int(1) DEFAULT NULL,
  `utitle` varchar(255) DEFAULT NULL,
  `udescription` text,
  `ukeywords` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_c_admindatatypes
-- ----------------------------
INSERT INTO `it_c_admindatatypes` VALUES ('1', '233', null, '', '', '');

-- ----------------------------
-- Table structure for `it_c_adminfm`
-- ----------------------------
DROP TABLE IF EXISTS `it_c_adminfm`;
CREATE TABLE `it_c_adminfm` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `visible` int(1) DEFAULT NULL,
  `utitle` varchar(255) DEFAULT NULL,
  `udescription` text,
  `ukeywords` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_c_adminfm
-- ----------------------------
INSERT INTO `it_c_adminfm` VALUES ('1', '232', null, '', '', '');

-- ----------------------------
-- Table structure for `it_c_adminmoduleorders`
-- ----------------------------
DROP TABLE IF EXISTS `it_c_adminmoduleorders`;
CREATE TABLE `it_c_adminmoduleorders` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `utitle` varchar(255) DEFAULT NULL,
  `udescription` text,
  `ukeywords` text,
  `block` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_c_adminmoduleorders
-- ----------------------------
INSERT INTO `it_c_adminmoduleorders` VALUES ('1', '399', '', '', '', 'order');

-- ----------------------------
-- Table structure for `it_c_adminusers`
-- ----------------------------
DROP TABLE IF EXISTS `it_c_adminusers`;
CREATE TABLE `it_c_adminusers` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `visible` int(1) DEFAULT NULL,
  `utitle` varchar(255) DEFAULT NULL,
  `udescription` text,
  `ukeywords` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_c_adminusers
-- ----------------------------
INSERT INTO `it_c_adminusers` VALUES ('1', '234', null, '', '', '');

-- ----------------------------
-- Table structure for `it_c_contacts`
-- ----------------------------
DROP TABLE IF EXISTS `it_c_contacts`;
CREATE TABLE `it_c_contacts` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `utitle` varchar(255) DEFAULT NULL,
  `udescription` text,
  `ukeywords` text,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `map` text,
  `admin_mail` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_c_contacts
-- ----------------------------
INSERT INTO `it_c_contacts` VALUES ('1', '47', 'Контакты', 'Контакты', 'Контакты', 'Россия, г. Екатеринбург, 620075, ул.Восточная 80, офис 3', '+7 343-253-22-00', '<script type=\"text/javascript\" charset=\"utf-8\" src=\"https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=RGnTa7SxMN3jWOy_W1ufGZrLay3BdQdL&width=500&height=400&lang=ru_RU&sourceType=constructor\"></script>', 'boris@elgrow.ru');

-- ----------------------------
-- Table structure for `it_c_e404`
-- ----------------------------
DROP TABLE IF EXISTS `it_c_e404`;
CREATE TABLE `it_c_e404` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `utitle` varchar(255) DEFAULT NULL,
  `udescription` text,
  `ukeywords` text,
  `text` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_c_e404
-- ----------------------------
INSERT INTO `it_c_e404` VALUES ('3', '13', '', '', '', '<p>Приносим свои извинения, но запрашиваемая страница не найдена.</p>');

-- ----------------------------
-- Table structure for `it_c_first`
-- ----------------------------
DROP TABLE IF EXISTS `it_c_first`;
CREATE TABLE `it_c_first` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `visible` int(1) DEFAULT NULL,
  `utitle` varchar(255) DEFAULT NULL,
  `udescription` text,
  `ukeywords` text,
  `seoname` varchar(255) DEFAULT NULL,
  `seotext` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_c_first
-- ----------------------------
INSERT INTO `it_c_first` VALUES ('6', '1', null, '', '', '', 'ОБ OLLI TOURS', '<p>Компания &ldquo;Олли турс&rdquo; на Российском рынке туристических услуг с 2013 года. Мы предлагает Вам отдыхать активно, ведь жизнь &ndash; это не только работа, а отдых &ndash; это не только пляж и шведский стол. Именно активный отдых &ndash; это самые яркие эмоции, новые ощущения, мысли и знания!</p>\r\n<p>Мы поможем наполнить ваше путешествие незабываемыми впечатлениями! Наша компания предлагает большой набор недорогих пешеходных и велосипедных туров, доступных для любого человека, не зависимо от возраста и уровня подготовки.Чешское представительство нашей компании находится в городе Сланы. Чехию по праву можно назвать раем для велосипедистов. В этом нет ничего удивительного, где еще можно покататься так , чтобы было не дорого, не холодно и не жарко, не сильно гористо и безопасно? Мягкий климат позволяет кататься на велосипеде с конца марта по конец октября.</p>\r\n<p>Вы не <strong>считаете</strong> себя &nbsp;заядлым велосипедистом и спортсменом? Ничего страшного! Относительно ровный рельеф местности делает велосипедные прогулки легкими и приятными. В то же время, если вы уже подготовленный, опытный велосипедист, выбирайте маршрут по холмистой местности, проехав по которому, вы сможете не только полюбоваться прекрасными видами, но и проверить себя на выносливость.</p>\r\n<p>В Чехии активно развивается велосипедный туризм и очень скоро она составит серьезную конкуренцию другим велосипедным европейским странам. Все больше путешественников выбирают велосипед как средство передвижения во время своих путешествий. Велосипедные экскурсии привлекают туристов особым ощущением свободы и возможностью поближе познакомиться со страной.</p>\r\n<p>Не менее интересны наши экскурсии со скандинавскими палочками. По последним подсчетам в мире более 10 миллионов поклонников скандинавской ходьбы! Присоединяйтесь!</p>\r\n<p>&laquo;Если вы выходите на прогулку без палок в некоторых странах Европы, то вы выглядите странно&raquo;, - говорил Пит Эдвардс, основатель сайта о скандинавской ходьбе. Уникальность скандинавской ходьбы заключается в том, что она подходит для туристов всех возрастов. Попробуйте хоть раз пройтись с скандинавскими палочками и вы поймете, что это не только увлекательно, но и очень полезно для здоровья. Наши экскурсии проводят сертифицированные русские гиды, которые не просто сухо излагают исторический материал, а доступным языком рассказывают о понятных и интересных фактах.</p>\r\n<p>Отдыхайте с Олли турс, мы превратим ваш отпуск в незабываемое приключение!</p>');

-- ----------------------------
-- Table structure for `it_c_manage`
-- ----------------------------
DROP TABLE IF EXISTS `it_c_manage`;
CREATE TABLE `it_c_manage` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `visible` int(1) DEFAULT NULL,
  `utitle` varchar(255) CHARACTER SET cp1251 DEFAULT NULL,
  `udescription` text,
  `ukeywords` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_c_manage
-- ----------------------------
INSERT INTO `it_c_manage` VALUES ('3', '212', null, null, null, null);

-- ----------------------------
-- Table structure for `it_c_map`
-- ----------------------------
DROP TABLE IF EXISTS `it_c_map`;
CREATE TABLE `it_c_map` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `utitle` varchar(255) DEFAULT NULL,
  `udescription` text,
  `ukeywords` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_c_map
-- ----------------------------

-- ----------------------------
-- Table structure for `it_c_news`
-- ----------------------------
DROP TABLE IF EXISTS `it_c_news`;
CREATE TABLE `it_c_news` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `utitle` varchar(255) DEFAULT NULL,
  `udescription` text,
  `ukeywords` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_c_news
-- ----------------------------
INSERT INTO `it_c_news` VALUES ('2', '46', 'Новости', 'Новости', 'Новости');

-- ----------------------------
-- Table structure for `it_c_os`
-- ----------------------------
DROP TABLE IF EXISTS `it_c_os`;
CREATE TABLE `it_c_os` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `utitle` varchar(255) DEFAULT NULL,
  `udescription` text,
  `ukeywords` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_c_os
-- ----------------------------
INSERT INTO `it_c_os` VALUES ('2', '14', '', '', '');

-- ----------------------------
-- Table structure for `it_c_partners`
-- ----------------------------
DROP TABLE IF EXISTS `it_c_partners`;
CREATE TABLE `it_c_partners` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `utitle` varchar(255) DEFAULT NULL,
  `udescription` text,
  `ukeywords` text,
  `page_title` varchar(255) DEFAULT NULL,
  `page_text` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_c_partners
-- ----------------------------
INSERT INTO `it_c_partners` VALUES ('1', '40', 'Партнеры', 'Партнеры', 'Партнеры', 'Наши партнеры', 'Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот, несмотря на название, не имеет никакого отношения к обитателям водоемов.');

-- ----------------------------
-- Table structure for `it_c_phoneblock`
-- ----------------------------
DROP TABLE IF EXISTS `it_c_phoneblock`;
CREATE TABLE `it_c_phoneblock` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `utitle` varchar(255) DEFAULT NULL,
  `udescription` text,
  `ukeywords` text,
  `phone` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_c_phoneblock
-- ----------------------------

-- ----------------------------
-- Table structure for `it_c_projects`
-- ----------------------------
DROP TABLE IF EXISTS `it_c_projects`;
CREATE TABLE `it_c_projects` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `utitle` varchar(255) DEFAULT NULL,
  `udescription` text,
  `ukeywords` text,
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_c_projects
-- ----------------------------
INSERT INTO `it_c_projects` VALUES ('1', '41', 'Проекты', 'Проекты', 'Проекты', null, null);
INSERT INTO `it_c_projects` VALUES ('2', '42', 'Нефтяная сфера', 'Нефтяная сфера', 'Нефтяная сфера', 'Нефтяная сфера', 'Деньги власть Деньги власть Деньги власть Деньги власть Деньги власть Деньги власть Деньги власть Деньги власть Деньги власть Деньги власть Деньги власть ');
INSERT INTO `it_c_projects` VALUES ('3', '43', 'Газовая сфера', 'Газовая сфера', 'Газовая сфера', 'Газовая сфера', 'Газовая сфера');
INSERT INTO `it_c_projects` VALUES ('5', '45', 'Остальные', 'Остальные', 'Остальные', 'Остальные', 'Остальные');

-- ----------------------------
-- Table structure for `it_c_settings`
-- ----------------------------
DROP TABLE IF EXISTS `it_c_settings`;
CREATE TABLE `it_c_settings` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `utitle` varchar(255) DEFAULT NULL,
  `udescription` text,
  `ukeywords` text,
  `sitename` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `copy` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_c_settings
-- ----------------------------
INSERT INTO `it_c_settings` VALUES ('1', '12', '', '', '', 'Название сайта', '6319432@gmail.com', '+7 (343)', '234-56-78', '© Elgrow 2014. Все права защищены.');

-- ----------------------------
-- Table structure for `it_c_tablech`
-- ----------------------------
DROP TABLE IF EXISTS `it_c_tablech`;
CREATE TABLE `it_c_tablech` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `utitle` varchar(255) DEFAULT NULL,
  `udescription` text,
  `ukeywords` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_c_tablech
-- ----------------------------

-- ----------------------------
-- Table structure for `it_c_tablechitem`
-- ----------------------------
DROP TABLE IF EXISTS `it_c_tablechitem`;
CREATE TABLE `it_c_tablechitem` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `utitle` varchar(255) DEFAULT NULL,
  `udescription` text,
  `ukeywords` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_c_tablechitem
-- ----------------------------

-- ----------------------------
-- Table structure for `it_c_typo`
-- ----------------------------
DROP TABLE IF EXISTS `it_c_typo`;
CREATE TABLE `it_c_typo` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `visible` int(1) DEFAULT NULL,
  `utitle` varchar(255) CHARACTER SET cp1251 DEFAULT NULL,
  `udescription` text,
  `ukeywords` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=105 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_c_typo
-- ----------------------------
INSERT INTO `it_c_typo` VALUES ('6', '62', null, '', '', '');
INSERT INTO `it_c_typo` VALUES ('18', '59', null, '', '', '');
INSERT INTO `it_c_typo` VALUES ('4', '59', null, '', '', '');
INSERT INTO `it_c_typo` VALUES ('9', '95', null, '', '', '');
INSERT INTO `it_c_typo` VALUES ('13', '145', null, '', '', '');
INSERT INTO `it_c_typo` VALUES ('14', '146', null, '', '', '');
INSERT INTO `it_c_typo` VALUES ('19', '214', null, 'Новая страница', 'Описание', 'Новизна, страница');
INSERT INTO `it_c_typo` VALUES ('20', '215', null, null, null, null);
INSERT INTO `it_c_typo` VALUES ('22', '226', null, null, null, null);
INSERT INTO `it_c_typo` VALUES ('23', '228', null, null, null, null);
INSERT INTO `it_c_typo` VALUES ('25', '217', null, 'hdfhdf', 'hdshsdhsd', 'dshsdhsd');
INSERT INTO `it_c_typo` VALUES ('26', '236', null, '', '', '');
INSERT INTO `it_c_typo` VALUES ('27', '240', null, 'Услуги психолога, стоимость услуг психолога в Екатеринбурге, психологическое консультирование цены', 'Стоимость услуг психолога в Екатеринбурге, 8-953-388-58-09', 'цена, стоимость, услуги, психолог, психоаналитик, телесный');
INSERT INTO `it_c_typo` VALUES ('28', '241', null, '', '', '');
INSERT INTO `it_c_typo` VALUES ('29', '242', null, '', '', '');
INSERT INTO `it_c_typo` VALUES ('30', '243', null, '', '', '');
INSERT INTO `it_c_typo` VALUES ('31', '245', null, 'Книги по психологии, Екатеринбург', 'Книги по психологии в Екатеринбурге', 'Книги по психологии, литература');
INSERT INTO `it_c_typo` VALUES ('32', '246', null, 'Фромм Дианетика', 'Фромм Дианетика', 'Фромм Дианетика');
INSERT INTO `it_c_typo` VALUES ('33', '247', null, 'Альбисетти, Истинная любовь', 'Альбисетти, Истинная любовь', 'Альбисетти, Истинная любовь');
INSERT INTO `it_c_typo` VALUES ('34', '248', null, 'Ирвин Ялом Лечение от любви и другие психотерапевтические новеллы', '', 'Лечение от любви и другие психотерапевтические новеллы');
INSERT INTO `it_c_typo` VALUES ('35', '249', null, 'Джонсон О мифах', '', 'Джонсон О мифах');
INSERT INTO `it_c_typo` VALUES ('36', '250', null, 'Холлис Двое встреча и расставание', '', 'Холлис Двое встреча и расставание');
INSERT INTO `it_c_typo` VALUES ('37', '251', null, 'А. Джонсон Введение в историю о Священной Чаше', '', 'А. Джонсон Введение в историю о Священной Чаше');
INSERT INTO `it_c_typo` VALUES ('38', '252', null, 'Любовь или влюбленность Из книги Роберта А. Джонсона «ОНА. Глубинные аспекты женской психологии»', '', 'Любовь или влюбленность\r\n');
INSERT INTO `it_c_typo` VALUES ('39', '253', null, 'Болен Дж. Ш. В каждом мужчине обитают боги', '', 'Болен Дж. Ш. В каждом мужчине обитают боги');
INSERT INTO `it_c_typo` VALUES ('40', '254', null, 'Нойманн. Красавица и чудовище', '', 'Нойманн. Красавица и чудовище');
INSERT INTO `it_c_typo` VALUES ('41', '255', null, 'Эстес. ОХОТА КОГДА СЕРДЦЕ – ОДИНОКИЙ ОХОТНИК', '', 'Эстес. ОХОТА КОГДА СЕРДЦЕ – ОДИНОКИЙ ОХОТНИК');
INSERT INTO `it_c_typo` VALUES ('42', '256', null, 'НЕКОТОРЫЕ ТИПИЧНЫЕ СЦЕНАРИИ', '', 'НЕКОТОРЫЕ ТИПИЧНЫЕ СЦЕНАРИИ\r\nГлава из книги Эрика Берна «ЛЮДИ, КОТОРЫЕ ИГРАЮТ В ИГРЫ\r\nПСИХОЛОГИЯ ЧЕЛОВЕЧЕСКОЙ СУДЬБЫ»\r\n');
INSERT INTO `it_c_typo` VALUES ('43', '257', null, 'КАК МЫ СОЗДАЕМ ЛИЧНЫЕ ГРАНИЦЫ', '', 'КАК МЫ СОЗДАЕМ ЛИЧНЫЕ ГРАНИЦЫ\r\nГлава из книги Клауда Генри «Изменения, которые исцеляют»');
INSERT INTO `it_c_typo` VALUES ('44', '258', null, 'Главы из книги Лобзина В.С., Решетникова М.М. ', '', 'Главы из книги Лобзина В.С., Решетникова М.М. \r\n«Аутогенная тренировка»');
INSERT INTO `it_c_typo` VALUES ('45', '259', null, '15 Рейнуотер ИСКУССТВО', '', 'ИСКУССТВО САМОНАБЛЮДЕНИЯ\r\nГлава из книги Джанетт Рейнуотер. «ЭТО В ВАШИХ СИЛАХ.\r\nКак стать собственным психотерапевтом»');
INSERT INTO `it_c_typo` VALUES ('46', '260', null, 'Главы из книги Карен Хорни  «Невротическая личность нашего времени»', '', 'Главы из книги Карен Хорни  «Невротическая личность нашего времени»');
INSERT INTO `it_c_typo` VALUES ('47', '261', null, 'Болезненная зависимость', '', 'Болезненная зависимость\r\n\r\nГлава из книги Хорни К. «Невроз и личностный рост. Борьба за самореализацию»');
INSERT INTO `it_c_typo` VALUES ('48', '262', null, 'Движущие силы неврозов.', '', 'Движущие силы неврозов.\r\nГлава из книги Карен Хорни «Самоанализ»\r\n');
INSERT INTO `it_c_typo` VALUES ('49', '263', null, '', '', '');
INSERT INTO `it_c_typo` VALUES ('50', '264', null, 'Роль подчиненной функции в психическом развитии', '', 'Роль подчиненной функции в психическом развитии\r\n\r\nГлава из книги «Лекции по юнговской типологии». \r\nФон Франц М.-.Л. «Подчиненная функция»');
INSERT INTO `it_c_typo` VALUES ('51', '265', null, 'Определения Юнга', '', 'Определения Юнга\r\nГлава из книги «Лекции по юнговской типологии». Джеймс  Хиллман.  «Чувствующая функция»');
INSERT INTO `it_c_typo` VALUES ('52', '266', null, 'Типы личности', '', 'Типы личности');
INSERT INTO `it_c_typo` VALUES ('53', '267', null, 'Психологический словарь А-Б-В - Екатеринбург', 'Психологический словарь', 'Психологический словарь');
INSERT INTO `it_c_typo` VALUES ('54', '268', null, '', '', '');
INSERT INTO `it_c_typo` VALUES ('55', '269', null, '', '', '');
INSERT INTO `it_c_typo` VALUES ('56', '270', null, '', '', '');
INSERT INTO `it_c_typo` VALUES ('57', '271', null, '', '', '');
INSERT INTO `it_c_typo` VALUES ('58', '272', null, '', '', '');
INSERT INTO `it_c_typo` VALUES ('59', '273', null, '', '', '');
INSERT INTO `it_c_typo` VALUES ('60', '274', null, 'Миф о Психее: метафора психологического развития', '', 'Миф о Психее: метафора психологического развития\r\nГлава из книги Джин Шиноды Болен\r\n«Богини в каждой женщине.\r\nНОВАЯ ПСИХОЛОГИЯ ЖЕНЩИНЫ. АРХЕТИПЫ БОГИНЬ»');
INSERT INTO `it_c_typo` VALUES ('61', '275', null, 'Яд ужасной матери', 'Яд ужасной матери\r\nГлава из книги Сибилл Биркхойзер-Оэри\r\n', 'Яд ужасной матери\r\nГлава из книги Сибилл Биркхойзер-Оэри\r\n');
INSERT INTO `it_c_typo` VALUES ('62', '276', null, 'ЗАВИСИМОСТЬ И ЗАБОТА О РЕБЕНКЕ', '', 'ЗАВИСИМОСТЬ И ЗАБОТА О РЕБЕНКЕ\r\nГлава из книги Дональда Вудса Винникотта\r\n');
INSERT INTO `it_c_typo` VALUES ('63', '279', null, 'Тесты по психологии, Екатеринбург', '', '');
INSERT INTO `it_c_typo` VALUES ('64', '280', null, 'Тест Кэтелла', '', '');
INSERT INTO `it_c_typo` VALUES ('65', '281', null, 'Thematic Apperception Test', '', 'Thematic Apperception Test');
INSERT INTO `it_c_typo` VALUES ('66', '282', null, '', '', '');
INSERT INTO `it_c_typo` VALUES ('67', '283', null, '', '', '');
INSERT INTO `it_c_typo` VALUES ('68', '284', null, 'Психолог для тинейджера', '', '');
INSERT INTO `it_c_typo` VALUES ('69', '285', null, '', '', '');
INSERT INTO `it_c_typo` VALUES ('70', '286', null, '', '', '');
INSERT INTO `it_c_typo` VALUES ('71', '287', null, '', '', '');
INSERT INTO `it_c_typo` VALUES ('72', '288', null, 'Психологический словарь Г-Д-Е - Екатеринбург', 'Психологический словарь', 'Психологический словарь');
INSERT INTO `it_c_typo` VALUES ('73', '289', null, '', '', '');

-- ----------------------------
-- Table structure for `it_c_users`
-- ----------------------------
DROP TABLE IF EXISTS `it_c_users`;
CREATE TABLE `it_c_users` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `parent` int(12) DEFAULT NULL,
  `utitle` varchar(255) DEFAULT NULL,
  `udescription` text,
  `ukeywords` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_c_users
-- ----------------------------
INSERT INTO `it_c_users` VALUES ('1', '29', '', '', '');

-- ----------------------------
-- Table structure for `it_datatypes`
-- ----------------------------
DROP TABLE IF EXISTS `it_datatypes`;
CREATE TABLE `it_datatypes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_datatypes
-- ----------------------------
INSERT INTO `it_datatypes` VALUES ('9', 'text', 'однострочный текст');
INSERT INTO `it_datatypes` VALUES ('7', 'select', 'выпадающий список с единственным выбором');
INSERT INTO `it_datatypes` VALUES ('8', 'mselect', 'список с множественным выбором');
INSERT INTO `it_datatypes` VALUES ('13', 'checkbox', 'триггер');
INSERT INTO `it_datatypes` VALUES ('14', 'html', 'html код');
INSERT INTO `it_datatypes` VALUES ('16', 'file', 'поле для загрузки файла');
INSERT INTO `it_datatypes` VALUES ('18', 'date', 'дата');
INSERT INTO `it_datatypes` VALUES ('19', 'items', 'вложенные блоки');
INSERT INTO `it_datatypes` VALUES ('33', 'mapfrom', 'Карта сайта от элемента');
INSERT INTO `it_datatypes` VALUES ('35', 'time', 'Время');
INSERT INTO `it_datatypes` VALUES ('36', 'counter', 'Многострочный текст без форматирования');
INSERT INTO `it_datatypes` VALUES ('45', 'imageload', 'Загрузка изображений');
INSERT INTO `it_datatypes` VALUES ('42', 'features', 'Характеристики');
INSERT INTO `it_datatypes` VALUES ('46', 'dates', 'Даты');
INSERT INTO `it_datatypes` VALUES ('47', 'gmap', 'Координаты на карте');

-- ----------------------------
-- Table structure for `it_rt`
-- ----------------------------
DROP TABLE IF EXISTS `it_rt`;
CREATE TABLE `it_rt` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `aid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `super` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_rt
-- ----------------------------
INSERT INTO `it_rt` VALUES ('1', '1', '1');
INSERT INTO `it_rt` VALUES ('21', '23', '0');

-- ----------------------------
-- Table structure for `it_sadmin`
-- ----------------------------
DROP TABLE IF EXISTS `it_sadmin`;
CREATE TABLE `it_sadmin` (
  `admin_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(255) NOT NULL DEFAULT '',
  `admin_password` varchar(255) NOT NULL DEFAULT '',
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `canedit` varchar(255) NOT NULL DEFAULT '',
  `timezone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_sadmin
-- ----------------------------
INSERT INTO `it_sadmin` VALUES ('1', 'superadmin', 'a3653b49c4d8318ad5c0b621dd02be10', '1', '1', '', 'Asia/Yekaterinburg');
INSERT INTO `it_sadmin` VALUES ('23', 'admin', 'dbf7a0b68194ea5766496e4aea1049dd', '1', '2', '', 'Asia/Yekaterinburg');

-- ----------------------------
-- Table structure for `it_subs`
-- ----------------------------
DROP TABLE IF EXISTS `it_subs`;
CREATE TABLE `it_subs` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `msg` text NOT NULL,
  `file_name` varchar(200) NOT NULL DEFAULT '',
  `file_bin` longblob NOT NULL,
  `typ` int(1) NOT NULL DEFAULT '0',
  `dat` varchar(10) NOT NULL DEFAULT '',
  `arrmail` text NOT NULL,
  `arrstart` int(11) NOT NULL DEFAULT '0',
  `step` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_subs
-- ----------------------------

-- ----------------------------
-- Table structure for `it_tree`
-- ----------------------------
DROP TABLE IF EXISTS `it_tree`;
CREATE TABLE `it_tree` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent` bigint(20) NOT NULL DEFAULT '0',
  `level` tinyint(4) NOT NULL DEFAULT '0',
  `sort` bigint(20) NOT NULL DEFAULT '0',
  `visible` tinyint(4) NOT NULL DEFAULT '0',
  `left_key` bigint(20) NOT NULL DEFAULT '0',
  `right_key` bigint(20) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `key` varchar(255) NOT NULL DEFAULT '',
  `template` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `writeend` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_tree
-- ----------------------------
INSERT INTO `it_tree` VALUES ('1', '0', '0', '0', '1', '1', '42', 'Главная страница', 'main', 'first', '/', '1');
INSERT INTO `it_tree` VALUES ('29', '1', '1', '15', '0', '2', '3', 'Пользователи', 'users', 'users', 'users/', '1');
INSERT INTO `it_tree` VALUES ('14', '1', '1', '19', '0', '4', '5', 'Заказ звонока', 'os', 'os', 'os/', '1');
INSERT INTO `it_tree` VALUES ('12', '1', '1', '20', '0', '6', '7', 'Настройки сайта', '', 'settings', '12/', '1');
INSERT INTO `it_tree` VALUES ('13', '1', '1', '21', '0', '8', '9', 'Ошибка 404', 'error404', 'e404', 'error404/', '1');
INSERT INTO `it_tree` VALUES ('2', '1', '1', '22', '0', '10', '25', 'Админка', 'manage', 'manage', 'manage/', '1');
INSERT INTO `it_tree` VALUES ('6', '2', '2', '2', '0', '11', '12', 'Папка', 'catedit', 'admincatedit', 'manage/catedit/', '1');
INSERT INTO `it_tree` VALUES ('3', '2', '2', '3', '0', '13', '14', 'Блок', 'blockedit', 'adminblockedit', 'manage/blockedit/', '1');
INSERT INTO `it_tree` VALUES ('5', '2', '2', '4', '0', '15', '16', 'Шаблоны папок', 'cattemplate', 'admincattemplate', 'manage/cattemplate/', '1');
INSERT INTO `it_tree` VALUES ('4', '2', '2', '5', '0', '17', '18', 'Шаблоны блоков', 'blocktemplate', 'adminblocktemplate', 'manage/blocktemplate/', '1');
INSERT INTO `it_tree` VALUES ('8', '2', '2', '7', '0', '19', '20', 'Типы данных', 'admindatatypes', 'admindatatypes', 'manage/admindatatypes/', '1');
INSERT INTO `it_tree` VALUES ('9', '2', '2', '8', '0', '21', '22', 'Пользователи', 'users', 'adminusers', 'manage/users/', '1');
INSERT INTO `it_tree` VALUES ('10', '2', '2', '9', '0', '23', '24', 'Модуль Заказы', 'moduleorders', 'adminmoduleorders', 'manage/moduleorders/', '1');
INSERT INTO `it_tree` VALUES ('39', '1', '1', '23', '0', '26', '27', 'О компании', 'about', 'about', 'about/', '1');
INSERT INTO `it_tree` VALUES ('40', '1', '1', '24', '0', '28', '29', 'Партнеры', 'partners', 'partners', 'partners/', '1');
INSERT INTO `it_tree` VALUES ('41', '1', '1', '25', '0', '30', '37', 'Проекты', 'projects', 'projects', 'projects/', '1');
INSERT INTO `it_tree` VALUES ('42', '41', '2', '1', '0', '31', '32', 'Нефтяная сфера', 'neftyanaya-sfera', 'projects', 'projects/neftyanaya-sfera/', '1');
INSERT INTO `it_tree` VALUES ('43', '41', '2', '2', '0', '33', '34', 'Газовая сфера', 'gazovaya-sfera', 'projects', 'projects/gazovaya-sfera/', '1');
INSERT INTO `it_tree` VALUES ('45', '41', '2', '3', '0', '35', '36', 'Остальные', 'ostalnye', 'projects', 'projects/ostalnye/', '1');
INSERT INTO `it_tree` VALUES ('46', '1', '1', '26', '0', '38', '39', 'Новости', 'news', 'news', 'news/', '1');
INSERT INTO `it_tree` VALUES ('47', '1', '1', '27', '0', '40', '41', 'Контакты', 'contacts', 'contacts', 'contacts/', '1');

-- ----------------------------
-- Table structure for `it_urls`
-- ----------------------------
DROP TABLE IF EXISTS `it_urls`;
CREATE TABLE `it_urls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `realurl` varchar(255) NOT NULL,
  `template` varchar(255) NOT NULL,
  `blockid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it_urls
-- ----------------------------
INSERT INTO `it_urls` VALUES ('1', 'projects/rosneft/', 'projects/', 'projectlist', '1');
INSERT INTO `it_urls` VALUES ('2', 'projects/rosneft6/', 'projects/', 'projectlist', '2');
INSERT INTO `it_urls` VALUES ('3', 'projects/petrol/', 'projects/', 'projecttypes', '1');
INSERT INTO `it_urls` VALUES ('4', 'projects/neftyanaya-sfera/rosneft.html/', 'projects/neftyanaya-sfera/', 'projectlist', '3');
INSERT INTO `it_urls` VALUES ('5', 'projects/gazovaya-sfera/gazprom.html/', 'projects/gazovaya-sfera/', 'projectlist', '4');
INSERT INTO `it_urls` VALUES ('6', 'projects/neftyanaya-sfera/bp.html/', 'projects/neftyanaya-sfera/', 'projectlist', '5');
INSERT INTO `it_urls` VALUES ('7', 'projects/neftyanaya-sfera/sdfkhj/', 'projects/neftyanaya-sfera/', 'projectlist', '6');
INSERT INTO `it_urls` VALUES ('8', 'news/Новость 1/', 'news/', 'news', '1');
INSERT INTO `it_urls` VALUES ('9', 'news/Вертолет/', 'news/', 'news', '2');
INSERT INTO `it_urls` VALUES ('10', 'news/skajdh/', 'news/', 'news', '3');
INSERT INTO `it_urls` VALUES ('11', 'news/uyuysdt/', 'news/', 'news', '4');
INSERT INTO `it_urls` VALUES ('12', 'news/sdkfhjg/', 'news/', 'news', '6');
INSERT INTO `it_urls` VALUES ('13', 'news/lkyhj/', 'news/', 'news', '7');
INSERT INTO `it_urls` VALUES ('14', 'news/hjgjhvk/', 'news/', 'news', '8');
INSERT INTO `it_urls` VALUES ('15', 'news/kjhgfgiughjyu/', 'news/', 'news', '9');
INSERT INTO `it_urls` VALUES ('16', 'news/mvbmnvjhfuytru/', 'news/', 'news', '10');

-- ----------------------------
-- Table structure for `it__templates`
-- ----------------------------
DROP TABLE IF EXISTS `it__templates`;
CREATE TABLE `it__templates` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `page` varchar(255) NOT NULL DEFAULT '',
  `html` varchar(255) NOT NULL DEFAULT '',
  `flag` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of it__templates
-- ----------------------------
