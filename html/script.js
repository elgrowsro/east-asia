$(document).ready(function(){
    //Сворачивание меню
    $('#mobileClicker').click(function(){
        $('.mainmenu').toggleClass('active');
    });
    //Изображения на ABOUT
    function width(){
        var leftColumnheight = $('.leftColumn').height();
        $('.rightColumn').css({height:leftColumnheight});
    }
    width();
    setInterval(width,10);
    //Точки на карте и модальное окно
    function anim() {
        $('.modal').removeClass('anim');
    }
    function content(){
        
    }
    $('.markers>div').click(function(){
        setTimeout(anim, 700);
        if ($('.modal').hasClass('open')){
            $('.modal').addClass('anim');
        }
        $('.markers>div').removeClass('active');
        $(this).addClass('active');
        $('.modal').addClass('open');
        $('.modal .name, .modal .country, .modal .site, .modal .textmodal').html('');
        var name = $(this).attr('article');
        var country = $(this).attr('country');
        var site = $(this).attr('site');
        var text = $(this).attr('data');
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        setTimeout(function () {
            $('.modal .name').append(name);
            $('.modal .country').append('<b>Страна:</b> ' + country);
            $('.modal .site').append('<b>Сайт: </b><a target="partner" href="'+site+'">' + site + '</a>');
            $('.modal .textmodal').append(text);
        }, 300); 
       
    });
    $('.close').click(function(){
        $('.modal').removeClass('open');
        $('.markers>div').removeClass('active');
    });
    //Расстановка новостей
    var count = $('.newsBlock').length;
    var fullHeight = 0;
    function widthNews(){
        var fullWidth = $(window).width();
        if (fullHeight>fullHeight){
            fullHeight = 0;
        }else{
          fullHeight = $(window).height();  
        }
        
        var widthPatt = ((fullWidth*2/5))*Math.ceil(count/3);
        if (fullWidth>=700){
            $('.newsBlock').height((fullHeight-58)/3).width(fullWidth/2.5);
            $('.newsPoligone').height((fullHeight-58));
            $('.newsWrap').width(widthPatt);
        } 
    }
    widthNews();
    setInterval(widthNews,10);
    var i = count/3;
    while(i < (count/3*2)){
        $('.newsBlock').eq(i).addClass('secondRow');
        i++;
    }
//Скроллинг новостей
    if ($('.newsPoligone').length){
        var mn = 0;
        // Функция для добавления обработчика событий
        function addHandler(object, event, handler, useCapture) {
            if (object.addEventListener) {
                object.addEventListener(event, handler, useCapture ? useCapture : false);
            } else if (object.attachEvent) {
                object.attachEvent('on' + event, handler);
            } else alert("Add handler is not supported");
        }
        // Добавляем обработчики
        /* Gecko */
        addHandler(window, 'DOMMouseScroll', wheel);
        /* Opera */
        addHandler(window, 'mousewheel', wheel);
        /* IE */
        addHandler(document, 'mousewheel', wheel);
        // Обработчик события
        function wheel(event) {
            var fullWidth = $(window).width();
            if (fullWidth>=700){
                var delta; // Направление скролла
                // -1 - скролл вниз
                // 1  - скролл вверх
                event = event || window.event;
                // Opera и IE работают со свойством wheelDelta
                if (event.wheelDelta) {
                    delta = event.wheelDelta / 120;
                    // В Опере значение wheelDelta такое же, но с противоположным знаком
                    if (window.opera) delta = -delta;
                    // В реализации Gecko получим свойство detail
                } else if (event.detail) {
                    delta = -event.detail / 3;
                }
                var fullWidth = $(window).width();
                var widthPatt = ((fullWidth*2/5))*Math.ceil(count/3);
                if (delta==1){  
                    if (mn>0){}else{
                        $('.newsWrap').css({marginLeft:mn});
                        mn = mn+50;
                    }
                    
                }else{
                    if (mn>=-widthPatt+fullWidth){
                        $('.newsWrap').css({marginLeft:mn});
                        mn = mn-50;    
                    }        
                }
                // Запрещаем обработку события браузером по умолчанию
                if (event.preventDefault)  event.preventDefault();
                event.returnValue = false;
                return delta;
            }
        }
}
  
    //Скроллинг проектов
    var projCount = $('.project').length;
    var slideSNow = -1;
    var maxSlides = projCount - 4;
    if (projCount<5){
        $('.arrowR').hide();
    }
    $('.project').eq(0).addClass('v1');
    $('.project').eq(1).addClass('v2');
    $('.project').eq(2).addClass('v3');
    $('.project').eq(3).addClass('v4');
    var lastI = 3;
    $('.arrowR').click(function(){
        slideSNow = slideSNow+1;
        if (slideSNow<maxSlides + 4){
            $('.project').removeClass('v0');
            $('.project').removeClass('v1');
            $('.project').removeClass('v2');
            $('.project').removeClass('v3');
            $('.project').removeClass('v4');
             lastI = lastI + 1;
             $('.project').eq(lastI).addClass('v4');
            $('.project').eq(lastI-1).addClass('v3');
            $('.project').eq(lastI-2).addClass('v2');
            $('.project').eq(lastI-3).addClass('v1');
            $('.project').eq(lastI-4).addClass('v0');
        }
        if(maxSlides == slideSNow){
            $('.project').eq(0).addClass('v4');
        }
        if(maxSlides +1 == slideSNow){
            $('.project').eq(0).addClass('v3');
            $('.project').eq(1).addClass('v4');
        }
        if(maxSlides +2 == slideSNow){
            $('.project').eq(0).addClass('v2');
            $('.project').eq(1).addClass('v3');
            $('.project').eq(2).addClass('v4');
        }
        if(maxSlides +3 == slideSNow){
            $('.project').eq(0).addClass('v1');
            $('.project').eq(1).addClass('v2');
            $('.project').eq(2).addClass('v3');
            $('.project').eq(3).addClass('v4');
            slideSNow = -1;
            lastI = 3;
        }
        
    }); 
    //$('.arrowL').click(function(){
    //    if (slideSNow>0){
    //        slideSNow = slideSNow-1;
    //        if (slideSNow<maxSlides){
    //            $('.arrowR').show();
    //        }
    //        if (slideSNow==0){
    //            $('.arrowL').hide();
    //        }
    //        $('.project').removeClass('v0');
    //        $('.project').removeClass('v1');
    //        $('.project').removeClass('v2');
    //        $('.project').removeClass('v3');
    //        $('.project').removeClass('v4');
    //         lastI = lastI - 1;
    //         $('.project').eq(lastI).addClass('v4');
    //        $('.project').eq(lastI-1).addClass('v3');
    //        $('.project').eq(lastI-2).addClass('v2');
    //        $('.project').eq(lastI-3).addClass('v1');
    //        $('.project').eq(lastI-4).addClass('v0');
    //    }
    //}); 
    if ($('.project').length){
        var hashDefault = location.hash;
        if (hashDefault != ''){
            $('.close').show();
            $('.project').each(function(){
                $('.close').css({display:'block'});
                if ($(this).attr('href')==hashDefault){
                    $(this).addClass('open');
                    var hash = $(this).attr('href');
                    var hashnothash = hash.split('#');
                    $('.project').addClass('other');
                    $(this).find('.contentWrap').load('/'+hashnothash[1]+'.html');
                }
            });
        }
        $('.project').click(function(){
            $('.project').removeClass('closing');
            $('.contentWrap').html('');
            var hash = $(this).attr('href');
                $('.close').show();
                $(this).addClass('open');
                $('.project').addClass('other');
                var hashnothash = hash.split('#');
                $(this).find('.contentWrap').load('/'+hashnothash[1]+'.html', function( response, status, xhr ){if ( status == "error" ) {
                    alert ('Такой страницы не существует. Ничего не откроется.');
                        $('.project.open').addClass('closing');
                        $('.close').hide();
                        $('.project').removeClass('other');
                        $('.project.open').removeClass('open');
                        $('.project.open .content').animate({opacity:0},500,function(){
                            $('.project .content').remove();
                            $('.project').removeClass('closing');
                            
                        });
                }
                });
            
        });
          
        $('.projectsWrap .close').click(function(){
            //var href = location.href;
            //var now = href.split('#');
            //window.location.href = now[0];
            $('.project.open').addClass('closing');
            $('.close').hide();
            $('.project').removeClass('other');
            $('.project.open').removeClass('open');
            $('.project.open .content').animate({opacity:0},500,function(){
                $('.project .content').remove();
                $('.project').removeClass('closing');
            });
        });
    }
});