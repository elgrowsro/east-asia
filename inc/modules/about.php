<?php
class about {

	public function __construct() {
		global $control;
		if ($control->oper == 'view') {
			$this->printOne($control->bid);
		}
		else {
			$this->printList($control->module_parent);
		}
	}

	private function printOne($bid) {
		global $control;

		$sign = md5($control->template.$control->module_url.$control->urlparams);
		phpFastCache::$storage = "auto";
		$content = phpFastCache::get($sign);

		if ($content == null) {
			$page = all::b_data_all($bid, $control->module_wrap);

			$page->back = all::getUrl($control->module_parent).all::addUrl($this->page);
			$this->html['text'] = sprintt($page, 'templates/'.$control->template.'/'.$control->template.'_one.html');

			// Кешируем на 24 часа
			// phpFastCache::set($sign, $this->html['text'], 86400);
		}
		else {
			$this->html['text'] = $content;
		}
	}

	private function printList($cid) {
		//error_reporting(E_ALL);
		global $control;

		$sign = md5($control->template.$control->module_url.$control->urlparams);
		phpFastCache::$storage = "auto";
		$content = phpFastCache::get($sign);

		if ($content == null) {
			$first_page_id = 1;

			$page = all::c_data_all($control->cid, $control->template);

			//filelist
			$images = new Listing('imagelist','blocks',$control->module_parent);
			$images->getList();
			$images->getItem();
			$page->backgrounds = $images->item;
			foreach ($page->backgrounds as $image) {
				$image->image = trim($image->image[0]->image, ';');
			}

			//filelist
			$files = new Listing('filelist','blocks',$control->module_parent);
			$files->getList();
			$files->getItem();
			$page->files = $files->item;

			//texthtml
			$txts = new Listing('texthtml','blocks',$control->module_parent);
			$txts->getList();
			$txts->getItem();
			$page->txts = $txts->item;

			//mainmenu
			$menu = new Listing('mainmenu','blocks',$first_page_id);
			$menu->getList();
			$menu->getItem();
			$page->menu = $menu->item;
			//Выделяем пункт меню, в котором мы сейчас
			$sep = '://';
			$host_uri = substr($control->url, strpos($control->url, $sep) + strlen($sep));
			foreach ($page->menu as $item) {
				if (strnatcasecmp($host_uri, $_SERVER['HTTP_HOST'] . $item->url) == 0) {
					$item->active = 1;
				}
			}

			$list = new Listing($control->module_wrap, "blocks", $cid);
			$list->page = $control->page;
			$list->tmp_url = all::getUrl($control->module_parent);
			$list->getList();
			$list->getItem();
			$list->getPage();

			$page->item = $list->item;
			$page->page = $list->navigation;
			$page->url_last = $list->url_last;
			$page->url_p = $list->url_p;
			$page->url_n = $list->url_n;
			$page->url_next = $list->url_next;


			$page->name = $control->name;
			$page->pages_down = sprintt($page, 'templates/temps/pages_down.html');
			$this->html['text'] = sprintt($page, 'templates/'.$control->template.'/'.$control->template.'.html');

			// Кешируем на 24 часа
			// phpFastCache::set($sign, $this->html['text'], 86400);
		}
		else {
			$this->html['text'] = $content;
		}
	}
}
?>