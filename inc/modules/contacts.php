<?php
class contacts {

	public function __construct() {
		global $control;

		if ($control->oper == 'feedback') {
			if ($_POST) {
				$page = all::c_data_all($control->cid, $control->template);
				$page->admin_mail;
				$title = 'Отзыв с сайта' . $_SERVER['HTTP_HOST'];
				$message = <<<TXT
Имя: {$_POST['name']}
Почта: {$_POST['email']}
Сообщение: {$_POST['message']}
TXT;
				$response = array();
				if (mail($page->admin_mail, $title, $message)) {
					$response['message'] = 'Сообщение отправлено.';
					$response['status'] = 'success';
				} else {
					$response['message'] = 'Сообщение не отправлено.';
					$response['status'] = 'fail';
				}

				exit(json_encode($response));
			}
		} else {
			$this->printList($control->module_parent);
		}
	}

	private function printList($cid) {
		global $control;

		$sign = md5($control->template.$control->module_url.$control->urlparams);
		phpFastCache::$storage = "auto";
		$content = phpFastCache::get($sign);

		if ($content == null) {
			$page = all::c_data_all($control->cid, $control->template);

			$first_page_id = 1;
			//mainmenu
			$menu = new Listing('mainmenu','blocks',$first_page_id);
			$menu->getList();
			$menu->getItem();
			$page->menu = $menu->item;
			//Выделяем пункт меню, в котором мы сейчас
			$sep = '://';
			$host_uri = substr($control->url, strpos($control->url, $sep) + strlen($sep));
			foreach ($page->menu as $item) {
				if (strnatcasecmp($host_uri, $_SERVER['HTTP_HOST'] . $item->url) == 0) {
					$item->active = 1;
				}
			}

			$list = new Listing($control->module_wrap, "blocks", $cid);
			$list->page = $control->page;
			$list->tmp_url = all::getUrl($control->module_parent);
			$list->getList();
			$list->getItem();
			$list->getPage();

			$page->contacts = $list->item;

			$page->name = $control->name;
			$this->html['text'] = sprintt($page, 'templates/'.$control->template.'/'.$control->template.'.html');

			// Кешируем на 24 часа
			// phpFastCache::set($sign, $this->html['text'], 86400);
		}
		else {
			$this->html['text'] = $content;
		}
	}
}
?>