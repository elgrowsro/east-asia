<?php
class news {
	public $news_limit = 50;

	public function __construct() {
		global $control;
		$this->page = $control->page;
		if ($control->oper == 'view') {
			$this->printOne($control->bid);
		} elseif($control->oper == 'more' && $_GET['ajax']) {
			$this->getMoreNews($control->bid);
		} else {
			$this->printList($control->module_parent);
		}
	}

	private function printOne($bid) {
		global $control;
		$page = all::b_data_all($bid, $control->module_wrap);
		$photos = explode(';', $page->photo);
		foreach ($photos as $photo) {
			if ($photo) {
				$page->photo = $photo;
				break;
			}
		}
		$page->d_m_date = substr($page->date_2, 0, -4);
		$page->y_date = substr($page->date_2, -4);

		$first_page_id = 1;
		//mainmenu
		$menu = new Listing('mainmenu','blocks',$first_page_id);
		$menu->getList();
		$menu->getItem();
		$page->menu = $menu->item;
		//Выделяем пункт меню, в котором мы сейчас
		$sep = '://';
		$host_uri = substr($control->url, strpos($control->url, $sep) + strlen($sep));
		foreach ($page->menu as $item) {
			if (strnatcasecmp($host_uri, $_SERVER['HTTP_HOST'] . $item->url) == 0) {
				$item->active = 1;
			}
		}

		$bid = (int)$bid;
		$cri = " id<>$bid AND ";
		$list = new Listing('news','blocks',$control->module_parent, $cri);
		$list->limit = 6;
		$list->sortfield = 'date';
		$list->sortby ='desc';
		$list->getList();
		$list->getItem();
		foreach ($list->item as $value) {
			$value->photo = trim($value->photo[0]->image, ';');
			$value->d_m_date = substr($value->date_2, 0, -4);
			$value->y_date = substr($value->date_2, -4);
		}
		$page->item = $list->item;

		$page->back = all::getUrl($control->module_parent).all::addUrl($this->page);
		$this->html['text'] = sprintt($page, 'templates/'.$control->template.'/'.$control->template.'_one.html');
	}

	private function printList($cid) {
		global $control;

		$first_page_id = 1;
		$page = new stdClass();

		//mainmenu
		$menu = new Listing('mainmenu','blocks',$first_page_id);
		$menu->getList();
		$menu->getItem();
		$page->menu = $menu->item;
		//Выделяем пункт меню, в котором мы сейчас
		$sep = '://';
		$host_uri = substr($control->url, strpos($control->url, $sep) + strlen($sep));
		foreach ($page->menu as $item) {
			if (strnatcasecmp($host_uri, $_SERVER['HTTP_HOST'] . $item->url) == 0) {
				$item->active = 1;
			}
		}

		$list = new Listing('news', 'blocks', $cid);
		$list->limit = $this->news_limit;
		$list->page = $control->page;
		$list->sortfield = 'date';
		$list->sortby ='desc';
		$list->tmp_url = all::getUrl($control->module_parent);
		$list->getList();
		$list->getItem();
		$list->getPage();
		foreach ($list->item as $value) {
			$value->photo = trim($value->photo[0]->image, ';');
			$value->d_m_date = substr($value->date_2, 0, -4);
			$value->y_date = substr($value->date_2, -4);
		}

		$page->item = $list->item;
		$page->page = $list->navigation;
		$page->url_last = $list->url_last;
		$page->url_p = $list->url_p;
		$page->url_n = $list->url_n;
		$page->url_next = $list->url_next;

		foreach ($page->item as $key => $val) {
			$date = $val->date_2;
			$dateArr = explode(" ", $date);
			$page->item[$key]->date_day = $dateArr[0];
			$page->item[$key]->date_month = $dateArr[1];
			if ($dateArr[2] < date("Y")) {
				$page->item[$key]->date_month .= " ".$dateArr[2];
			}
		}

		$page->news_block = sprintt($page->item, 'templates/'.$control->template.'/'.$control->template.'_block.html');

		$this->html['text'] = sprintt($page, 'templates/'.$control->template.'/'.$control->template.'.html');
	}

	private function getMonth($num, $mode=null) {
		$arr = array("Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь");
		if ($mode == null) {
			$num = str_replace("0", "", $num);
			$num = intval($num);

			if ($num > 0 && $num < 13) {
				return $arr[$num-1];
			}
		}

		if ($mode != null) {
			if (in_array($num, $arr)) {
				return array_search($num, $arr);
			}
		}

	}

	private function getMoreNews($page_num = 0)
	{
		global $control;

		$page_num = (int)$page_num;
		if ($page_num < 1) $page_num = 1;

		$page = new stdClass();

		$list = new Listing('news', 'blocks', $control->cid);
		$list->limit = $this->news_limit;
		$list->page = $page_num;
		$list->sortfield = 'date';
		$list->sortby ='desc';
		$list->tmp_url = all::getUrl($control->module_parent);
		$list->getList();
		$list->getItem();
		$list->getPage();
		foreach ($list->item as $value) {
			$value->photo = trim($value->photo[0]->image, ';');
			$value->d_m_date = substr($value->date_2, 0, -4);
			$value->y_date = substr($value->date_2, -4);
		}

		$page->item = $list->item;

		foreach ($page->item as $key => $val) {
			$date = $val->date_2;
			$dateArr = explode(" ", $date);
			$page->item[$key]->date_day = $dateArr[0];
			$page->item[$key]->date_month = $dateArr[1];
			if ($dateArr[2] < date("Y")) {
				$page->item[$key]->date_month .= " ".$dateArr[2];
			}
		}

		$news_block = sprintt($page->item, 'templates/'.$control->template.'/'.$control->template.'_block.html');

		$return = array();
		$return['content'] = $news_block;
		if ($list->url_n) $return['next_page'] = 1;

		exit(json_encode($return));
	}
}
?>