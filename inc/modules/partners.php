<?php
class partners {

	public function __construct() {
		global $control;

		$this->printList($control->module_parent);
	}

	private function printList($cid) {
		global $control;

		$sign = md5($control->template.$control->module_url.$control->urlparams);
		phpFastCache::$storage = "auto";
		$content = phpFastCache::get($sign);

		if ($content == null) {
			$first_page_id = 1;

			$page = all::c_data_all($control->cid, $control->template);

			//parnerslist
			$partners = new Listing('partnerslist','blocks',$control->module_parent);
			$partners->getList();
			$partners->getItem();
			$page->partners = $partners->item;
			foreach ($page->partners as $partner) {
				$partner->logo = trim($partner->logo[0]->image, ';');
				//text will be inserted to <div data="HERE">
				$partner->text = str_replace('"', '\'', $partner->text);
			}

			//mainmenu
			$menu = new Listing('mainmenu','blocks',$first_page_id);
			$menu->getList();
			$menu->getItem();
			$page->menu = $menu->item;
			//Выделяем пункт меню, в котором мы сейчас
			$sep = '://';
			$host_uri = substr($control->url, strpos($control->url, $sep) + strlen($sep));
			foreach ($page->menu as $item) {
				if (strnatcasecmp($host_uri, $_SERVER['HTTP_HOST'] . $item->url) == 0) {
					$item->active = 1;
				}
			}

			$list = new Listing($control->module_wrap, "blocks", $cid);
			$list->page = $control->page;
			$list->tmp_url = all::getUrl($control->module_parent);
			$list->getList();
			$list->getItem();
			$list->getPage();


			$page->name = $control->name;
			$page->pages_down = sprintt($page, 'templates/temps/pages_down.html');
			$this->html['text'] = sprintt($page, 'templates/'.$control->template.'/'.$control->template.'.html');

			// Кешируем на 24 часа
			// phpFastCache::set($sign, $this->html['text'], 86400);
		}
		else {
			$this->html['text'] = $content;
		}
	}
}
?>