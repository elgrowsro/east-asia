<?php
class projects {
	public $projects_parent = 41;

	public function __construct() {
		global $control;
		if ($control->oper == 'view') {
			$this->printOne($control->bid);
		}
		else {
			$this->printList($control->module_parent);
		}
	}

	private function printOne($bid) {
		global $control;

		$projects_parent = $this->projects_parent;
		$sign = md5($control->template.$control->module_url.$control->urlparams);
		phpFastCache::$storage = "auto";
		$content = phpFastCache::get($sign);

		if ($content == null) {
			$page = all::b_data_all($bid, 'projectlist');

			$logo = explode(';', $page->logo2);
			foreach ($logo as $value) {
				if ($value) {
					$page->logo2 = $value;
				}
			}
			//files
			$files = explode(';', $page->files);
			if ((isset($files[0]) && $files[0]) || count($files) > 1) {
				$cri = ' id IN(';
				foreach ($files as $k => $val) {
					if ($k) {
						$cri .= ','. $val;
					} else {
						$cri .= $val;
					}
				}
				$cri .= ') AND';

					$files = new Listing('filelist','blocks',$projects_parent,$cri);
					$files->getList();
					$files->getItem();
					$page->files = $files->item;
			}

			$return = sprintt($page, 'templates/'.$control->template.'/'.$control->template.'_one.html');

			exit($return);

			// Кешируем на 24 часа
			// phpFastCache::set($sign, $this->html['text'], 86400);
		}
		else {
			$this->html['text'] = $content;
		}
	}

	private function printList($cid) {
		global $control;

		$sign = md5($control->template.$control->module_url.$control->urlparams);
		phpFastCache::$storage = "auto";
		$content = phpFastCache::get($sign);

		if ($content == null) {
			$first_page_id = 1;
			$projects_parent = $this->projects_parent;

			$page = all::c_data_all($control->cid, $control->template);

			//categories
			$query = 'SELECT * FROM prname_categories WHERE `parent` = "' . (int)$projects_parent . '" or `id` = "'.(int)$projects_parent.'"';
			$cat_res = sql::query($query);
			$categories = array();
			$parent_cat = array();
			while ($cat = sql::fetch_object($cat_res)) {
				if ($cat->id == $projects_parent) {
					$parent_cat = $cat;
					continue;
				}
				if ($cat->id == $control->cid) $cat->active = 1;
				$categories[] = $cat;
			}
			foreach ($categories as $cat) {
				$cat->url = '/' . $parent_cat->key . '/' . $cat->key;
			}
			$page->categories = $categories;

			//mainmenu
			$menu = new Listing('mainmenu','blocks',$first_page_id);
			$menu->getList();
			$menu->getItem();
			$page->menu = $menu->item;
			//Выделяем пункт меню, в котором мы сейчас
			$sep = '://';
			$host_uri = substr($control->url, strpos($control->url, $sep) + strlen($sep));
			foreach ($page->menu as $item) {
				if (strnatcasecmp($host_uri, $_SERVER['HTTP_HOST'] . $item->url) == 0) {
					$item->active = 1;
				}
			}

			//projects
			$projects = new Listing('projectlist','blocks',$control->cid);
			$projects->getList();
			$projects->getItem();
			$page->projects = $projects->item;
			foreach ($page->projects as $project) {
				$project->logo1 = trim($project->logo1[0]->image, ';');
				$project->logo2 = trim($project->logo2[0]->image, ';');
				$project->background = trim($project->background[0]->image, ';');
				$project->text = mb_substr($project->text, 0, 30) . '...';
			}

			//Выделяем пункт меню, в котором мы сейчас
			$sep = '://';
			$host_uri = substr($control->url, strpos($control->url, $sep) + strlen($sep));
			foreach ($page->menu as $item) {
				if (strnatcasecmp($host_uri, $_SERVER['HTTP_HOST'] . $item->url) == 0) {
					$item->active = 1;
				}
			}


			$page->name = $control->name;
			$page->pages_down = sprintt($page, 'templates/temps/pages_down.html');
			$this->html['text'] = sprintt($page, 'templates/'.$control->template.'/'.$control->template.'.html');

			// Кешируем на 24 часа
			// phpFastCache::set($sign, $this->html['text'], 86400);
		}
		else {
			$this->html['text'] = $content;
		}
	}
}