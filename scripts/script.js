$(document).ready(function(){
    $('#feedback-form').submit(function(e) {
        e.preventDefault();
        var $form = $('#feedback-form');
        var $submit = $form.find('#submit');

        $submit.attr('value', 'Отправка...');

        $.ajax({
            url: '_afeedback',
            method: 'post',
            dataType: 'json',
            data: $form.serialize(),
            beforeSend: function () {
                this.url = this.url + '/';
            },
            success: function (response) {
                if (response['status'] == 'success') {
                    $form[0].reset();
                }
                $submit.attr('value', response['message']);
            },
            error: function () {
                $submit.attr('value', 'Ошибка, отправить ещё');
            }
        });

        return false;
    });
    //Сворачивание меню
    $('#mobileClicker').click(function(){
        $('.mainmenu').toggleClass('active');
    });
    //Изображения на ABOUT
    function width(){
        var leftColumnheight = $('.leftColumn').height();
        $('.rightColumn').css({height:leftColumnheight});
    }
    width();
    setInterval(width,10);
    //Точки на карте и модальное окно
    function anim() {
        $('.modal').removeClass('anim');
    }
    $('.markers>div').click(function(){
        setTimeout(anim, 700);
        if ($('.modal').hasClass('open')){
            $('.modal').addClass('anim');
        }
        $('.markers>div').removeClass('active');
        $(this).addClass('active');
        $('.modal').addClass('open');
        $('.modal .name, .modal .country, .modal .site, .modal .textmodal').html('');
        var name = $(this).attr('article');
        var country = $(this).attr('country');
        var site = $(this).attr('site');
        var text = $(this).attr('data');
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        setTimeout(function () {
            $('.modal .name').append(name);
            $('.modal .country').append('<b>Страна:</b> ' + country);
            $('.modal .site').append('<b>Сайт: </b><a target="partner" href="'+site+'">' + site + '</a>');
            $('.modal .textmodal').append(text);
        }, 300);

    });
    $('.close').click(function(){
        $('.modal').removeClass('open');
        $('.markers>div').removeClass('active');
    });
    //Расстановка новостей
    var count = $('.newsBlock').length;
    var fullHeight = 0;
    function widthNews(){
        var fullWidth = $(window).width();
        if (fullHeight>fullHeight){
            fullHeight = 0;
        }else{
          fullHeight = $(window).height();
        }
        //console.log(fullHeight);
        var widthPatt = ((fullWidth*2/5))*Math.ceil(count/3);
        if (fullWidth>=700){
            $('.newsBlock').width(fullWidth/2.5);
            $('.newsWrap').width(widthPatt);
        }
    }

    widthNews();
    setInterval(widthNews,10);
    var i = Math.floor(count/3);
    var remainder = count/3 - i;
    if (remainder>0){
        i++;
        while(i < (count/3*2 + 1)){
        $('.newsBlock').eq(i).addClass('secondRow');
        i++;
        }
    }else{
       while(i < (count/3*2)){
        $('.newsBlock').eq(i).addClass('secondRow');
        i++;
        }
    }
            //Firefox

var mn=0;
function displaywheel(e){
	var evt=window.event || e //equalize event object
	var delta=evt.detail? evt.detail*(-1) : evt.wheelDelta //check for evt.detail first so Opera uses that instead of evt.wheelDelta
	//document.getElementById("i2").innerHTML=delta //delta returns +120 when wheel is scrolled up, -120 when scrolled down
    var fullWidth = $(window).width();
    var widthPatt = ((fullWidth*2/5))*Math.ceil(count/3);
    if (delta>0){
        if (mn>0){}else{
            $('.newsWrap').css({marginLeft:mn});
            mn = mn+50;
        }
    }else{
         if (mn>=-widthPatt+fullWidth){
            $('.newsWrap').css({marginLeft:mn});
            mn = mn-50;
        }
    }
return true
}




var mousewheelevt=(/Firefox/i.test(navigator.userAgent))? "DOMMouseScroll" : "mousewheel" //FF doesn't recognize mousewheel as of FF3.x

if (document.attachEvent) //if IE (and Opera depending on user setting)
	document.attachEvent("on"+mousewheelevt, displaywheel)
else if (document.addEventListener) //WC3 browsers
	document.addEventListener(mousewheelevt, displaywheel, false)


//Скроллинг новостей
    if ($('.newsPoligone').length){
        var mn = 0;
        // Функция для добавления обработчика событий
        function addHandler(object, event, handler, useCapture) {
            if (object.addEventListener) {
                object.addEventListener(event, handler, useCapture ? useCapture : false);
            } else if (object.attachEvent) {
                object.attachEvent('on' + event, handler);
            } else alert("Add handler is not supported");
        }
        // Добавляем обработчики
        /* Gecko */
        addHandler(window, 'DOMMouseScroll', wheel);
        /* Opera */
        addHandler(window, 'mousewheel', wheel);
        /* IE */
        addHandler(document, 'mousewheel', wheel);
        // Обработчик события
        //function wheel(event) {
        //    var fullWidth = $(window).width();
        //    if (fullWidth>=700){
        //        var delta; // Направление скролла
        //        // -1 - скролл вниз
        //        // 1  - скролл вверх
        //        event = event || window.event;
        //        // Opera и IE работают со свойством wheelDelta
        //        if (event.wheelDelta) {
        //            delta = event.wheelDelta / 120;
        //            // В Опере значение wheelDelta такое же, но с противоположным знаком
        //            if (window.opera) delta = -delta;
        //            // В реализации Gecko получим свойство detail
        //        } else if (event.detail) {
        //            delta = -event.detail / 3;
        //        }
        //        var fullWidth = $(window).width();
        //        var widthPatt = ((fullWidth*2/5))*Math.ceil(count/3);
        //        if (delta==1){
        //            if (mn>0){}else{
        //                $('.newsWrap').css({marginLeft:mn});
        //                mn = mn+50;
        //            }
        //
        //        }else{
        //            if (mn>=-widthPatt+fullWidth){
        //                $('.newsWrap').css({marginLeft:mn});
        //                mn = mn-50;
        //            }
        //        }
        //        // Запрещаем обработку события браузером по умолчанию
        //        if (event.preventDefault)  event.preventDefault();
        //        event.returnValue = false;
        //        return delta;
        //    }
        //}



}

    //Скроллинг проектов
    var projCount = $('.project').length;
    var slideSNow = -1;
    var maxSlides = projCount - 4;
    if (projCount<5){
        $('.arrowR').hide();
    }
    $('.project').eq(0).addClass('v1');
    $('.project').eq(1).addClass('v2');
    $('.project').eq(2).addClass('v3');
    $('.project').eq(3).addClass('v4');
    var lastI = 3;
    $('.arrowR').click(function(){
        slideSNow = slideSNow+1;
        if (slideSNow<maxSlides + 4){
            $('.project').removeClass('v0');
            $('.project').removeClass('v1');
            $('.project').removeClass('v2');
            $('.project').removeClass('v3');
            $('.project').removeClass('v4');
             lastI = lastI + 1;
             $('.project').eq(lastI).addClass('v4');
            $('.project').eq(lastI-1).addClass('v3');
            $('.project').eq(lastI-2).addClass('v2');
            $('.project').eq(lastI-3).addClass('v1');
            $('.project').eq(lastI-4).addClass('v0');
        }
        if(maxSlides == slideSNow){
            $('.project').eq(0).addClass('v4');
        }
        if(maxSlides +1 == slideSNow){
            $('.project').eq(0).addClass('v3');
            $('.project').eq(1).addClass('v4');
        }
        if(maxSlides +2 == slideSNow){
            $('.project').eq(0).addClass('v2');
            $('.project').eq(1).addClass('v3');
            $('.project').eq(2).addClass('v4');
        }
        if(maxSlides +3 == slideSNow){
            $('.project').eq(0).addClass('v1');
            $('.project').eq(1).addClass('v2');
            $('.project').eq(2).addClass('v3');
            $('.project').eq(3).addClass('v4');
            slideSNow = -1;
            lastI = 3;
        }

    });
    //$('.arrowL').click(function(){
    //    if (slideSNow>0){
    //        slideSNow = slideSNow-1;
    //        if (slideSNow<maxSlides){
    //            $('.arrowR').show();
    //        }
    //        if (slideSNow==0){
    //            $('.arrowL').hide();
    //        }
    //        $('.project').removeClass('v0');
    //        $('.project').removeClass('v1');
    //        $('.project').removeClass('v2');
    //        $('.project').removeClass('v3');
    //        $('.project').removeClass('v4');
    //         lastI = lastI - 1;
    //         $('.project').eq(lastI).addClass('v4');
    //        $('.project').eq(lastI-1).addClass('v3');
    //        $('.project').eq(lastI-2).addClass('v2');
    //        $('.project').eq(lastI-3).addClass('v1');
    //        $('.project').eq(lastI-4).addClass('v0');
    //    }
    //});
    if ($('.project').length){
        var hashDefault = location.hash;
        if (hashDefault != ''){
            $('.close').show();
            $('.project').each(function(){
                $('.close').css({display:'block'});
                if ($(this).attr('href')==hashDefault){
                     $('.loading5').show();
                    $(this).addClass('open noneanimate');
                    var hash = $(this).attr('href');
                    var hashnothash = hash.split('#');
                    $('.project').addClass('other');
                    $(this).find('.contentWrap').load(hashnothash[1], function( response, status, xhr ){
                    if ( status == "error" ) {
                    alert ('Такой страницы не существует. Ничего не откроется.');
                        $('.project.open').addClass('closing');
                        $('.close').hide();
                        $('.project').removeClass('other');
                        $('.project.open').removeClass('open');
                        $('.project.open .content').animate({opacity:0},500,function(){
                            $('.project .content').remove();
                            $('.project').removeClass('closing');

                        });
                    }else if(status=="success"){
                        $('.loading5').hide();
                    }
                    });
                }
            });
        }
        $('.project').click(function(){
            if ($(this).hasClass('open')){

            }else{
                $(this).find('.loading5').show();
               $('.project').removeClass('closing');
                $('.contentWrap').html('');
                var hash = $(this).attr('href');
                $('.close').show();
                $(this).addClass('open');
                $('.project').addClass('other');
                var hashnothash = hash.split('#');

                //fix disappearing contentWrap
                if(!$(this).find('.contentWrap').length) $(this).append('<div class="contentWrap"></div>');

                $(this).find('.contentWrap').load(hashnothash[1], function( response, status, xhr ){
                    if ( status == "error" ) {
                    alert ('Такой страницы не существует. Ничего не откроется.');
                        $('.project.open').addClass('closing');
                        $('.close').hide();
                        $('.project').removeClass('other');
                        $('.project.open').removeClass('open');
                        $('.project.open .content').animate({opacity:0},500,function(){
                            $('.project .content').remove();
                            $('.project').removeClass('closing');

                        });
                    }else if(status=="success"){
                        $('.loading5').hide();
                    }
                });
            }
        });

        $('.projectsWrap .close').click(function(){
            //var href = location.href;
            //var now = href.split('#');
            //window.location.href = now[0];
            $('.noneanimate').removeClass('noneanimate');
            $('.project.open').addClass('closing');
            $('.close').hide();
            $('.project').removeClass('other');
            $('.project.open').removeClass('open');
            $('.project.open .content').animate({opacity:0},500,function(){
                $('.project .content').remove();
                $('.project').removeClass('closing');
            });
        });
    }
});

function getMoreNews(object) {
    var page = parseInt(object.attr("data-page-id"));
    if (!page || object.attr("data-allow-exec") == "disallow") {
        return false;
    }

    var next_page = page + 1;

    $.ajax({
        url: '/news/_amore_b' + next_page + '/',
        method: 'get',
        dataType: 'html',
        data: {ajax: "1"},
        beforeSend: function () {
            this.url = this.url + '/';
            object.attr("data-allow-exec", 'disallow');
        },
        complete: function () {
            object.attr("data-allow-exec", 'allow');
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response['content']) {
                $('#newsWrapper').append(response['content']);
                object.attr("data-page-id", next_page);
                if (!response['next_page']) {
                    object.attr("data-page-id", 0);
                    object.attr("data-allow-exec", 'disallow');
                }
            }
        }
    });
    return false;
}